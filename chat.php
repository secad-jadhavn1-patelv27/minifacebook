<?php
  require('authenticate_chat.php');
  require('./src/Post.php');
  $statistics = getStatistics();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="src/Views/style.css">
    <!-- <script src="https://secad-team3-jadhavn1.minifacebook.com:4430/socket.io/socket.io.js"></script> -->
    <script src="https://secad-team3-patelv27.minifacebook.com:4430/socket.io/socket.io.js"></script>
    <style>
      .chat {
        color: black;
        font-size: 20px;

      }
    </style>
  </head>

  <body onload="startTime()" class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="./src/Views/profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/admin/list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/profile.php';">
                    <i class="fa fa-comment"></i>
                    <span>Profile</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Public Chat</h3>
            </div>

            <div class="panel-content panel-activity chat">
              <input type="hidden" id="usernameChat" value="<?php echo $_SESSION['username'];?>">
              Current time: <div id="clock"></div>
              <br>
              Type message and press Enter to Send: <br><input type = "text" id="message" size = "30" onkeypress="entertoSend(event)" onkeyup="myWebSocket.emit('typing')"/>
              
              <br><br>
              <div id="typing"></div>
                  Messages:
              <hr>
              <div class="comment_list" id="receivedmessage"></div>
            </div>
        </div> <!-- End of Activity Feed -->
    </div>

    <script>
      var usernameChat = $("#usernameChat").val();
    
      function startTime() {
        document.getElementById('clock').innerHTML = new Date();
        // $('#clock').innerHTML = new Date();
        setTimeout(startTime, 500);
      }
      function doSend(msg){
          if (myWebSocket) {
            myWebSocket.send(msg);
            console.log('Sent to server: ' +msg);
          }
        }
        function entertoSend(e){
          //alert("keycode =" + e.keyCode);
          if(e.keyCode==13){//enter key
            doSend(document.getElementById("message").value);
            document.getElementById("message").value = "";
          }
        }
    // $(document).ready(function() {

      // var myWebSocket = io.connect('192.168.56.104:4430');
      var myWebSocket = io.connect('192.168.56.102:4430');

      myWebSocket.onopen = function() { 
        console.log('WebSocket opened'); 
      }

      myWebSocket.emit("online", usernameChat);

        var sanitizeHTML = function(str) {
          var temp = document.createElement('div');
          temp.textContent = str;
          return temp.innerHTML;
        } 

        myWebSocket.on("message", function(msg) {
          console.log('Received from server: '+ msg);
          chatMsg = sanitizeHTML(msg).split("says:");
          chatUser = "<b>"+ chatMsg[0] +"</b>" + " says : "  + chatMsg[1];
          document.getElementById("receivedmessage").innerHTML += chatUser + "<br>";
          // $('#receivedmessage').innerHTML += sanitizeHTML(msg) + "<br>";
        });

        myWebSocket.on("online", function(msg) {
          console.log('Received from server: '+ msg);
          document.getElementById("receivedmessage").innerHTML += sanitizeHTML(msg) + "<br>";

        });

        myWebSocket.on("typing", function(msg) {
          document.getElementById("typing").innerHTML = msg + "<br>";
          /*setsession_createTimeout(
            function(){
              document.getElementById("typing").innerHTML = '<br>';
            } , 500);*/

          setTimeout(function(){
            document.getElementById("typing").innerHTML= "<br"
          }, 1000); 


        });

        myWebSocket.onclose = function() { 
          console.log('WebSocket closed');
        }

    </script>

  </body>
</html>
