-- Database and User creation 

-- CREATE database miniFacebook;

-- create user 'minifb'@'localhost' IDENTIFIED BY 'team3';

-- grant all on miniFacebook.* to 'minifb'@'localhost';

-- If table exists, delete it.
DROP TABLE IF EXISTS `users`;

-- Create a new table 'users'.
CREATE TABLE users(
	user_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name varchar(50) NOT NULL,
	nickname varchar(50),
	email varchar(50) NOT NULL UNIQUE,
	contact varchar(10), 
	location varchar(50),
	dob date NOT NULL,
	gender enum('F','M','O') NOT NULL,
	user_type enum('S', 'U') DEFAULT 'U',
	password varchar(50) NOT NULL,
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
 
-- If table exists, delete it.
DROP TABLE IF EXISTS `posts`;

-- Create a new table 'users'.
CREATE TABLE posts(
	post_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_id int(11) NOT NULL,
	content text NOT NULL,
	image_path varchar(100),
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE
);

-- If table exists, delete it.
DROP TABLE IF EXISTS `comments`;

-- Create a new table 'users'.
CREATE TABLE comments(
	comment_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	post_id int(11) NOT NULL,
	user_id int(11) NOT NULL,
	comment_text text NOT NULL,
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id), 
	FOREIGN KEY (post_id) REFERENCES posts(post_id) ON DELETE CASCADE
);

-- If table exists, delete it.
DROP TABLE IF EXISTS `likes`;

-- Create a new table 'users'.
CREATE TABLE likes(
	like_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	post_id int(11) NOT NULL,
	user_id int(11) NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id)
);