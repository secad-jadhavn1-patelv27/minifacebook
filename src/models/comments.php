<?php
	require('config/db.php');

	function addComment($data) {
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$post_id		= htmlspecialchars($data["post_id"]);
		$comment_text   = htmlspecialchars($data["comment"]);
		$status 		= htmlspecialchars("A");

		global $mysqli;

		$insertPost = "INSERT INTO comments(user_id, comment_text, post_id, status) VALUES (?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertPost))
			error_log("miniFacebook: " . "Post insert Prepared Statement Error!");

		$stmt->bind_param("isis", $user_id, $comment_text, $post_id, $status);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function editComment($commentid) { // Owner of comment, owner of post and superuser

	}

	function getOwnerType($commentid) {
		global $mysqli;
		$post_id= 0 ;

		$commentDetails = "SELECT user_id, post_id from comments WHERE comment_id = ?";

		if(!$stmt = $mysqli->prepare($commentDetails))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("i", $commentid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return "";
		}

		$stmt->bind_result($user_id, $post_id);

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return "";
		}
		error_log("Nirmala Comment Owner: ". $stmt->num_rows);

		if($stmt->num_rows == 1) {
			if($stmt->fetch())  {
				error_log("Nirmala Comment Owner: ". $stmt->num_rows ." ::: " . $user_id. "::". $post_id);
				if($_SESSION['user_id'] == $user_id)
					return "comment";
			}
		}

		if(isPostOwner($post_id)) {
			error_log("Nirmala Post Owner: ". $post_id);
			return "post";
		}

		return "";
	}

	function isPostOwner($post_id) {
		$user_id	 	= $_SESSION['user_id'];
		global $mysqli;

		$postDetails = "SELECT user_id from posts WHERE post_id = ? and user_id=?";

		if(!$stmt = $mysqli->prepare($postDetails))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("ii", $post_id, $user_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		if($stmt->num_rows == 1) {
			error_log("Vrunda Post owner");
			return true;
		}

		return false;
	}

	function deleteComment($commentid) { // Owner of comment, owner of post and superuser

		global $mysqli;

		$savePost = "DELETE from comments WHERE comment_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("i", $commentid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;

	}



?>