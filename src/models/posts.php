<?php
	require $domain.'config/db.php';

	function getAllPosts() { // All active posts 

		global $mysqli;

		$response = array();

		$selectUser = "SELECT 
				users.user_id as user_id, users.name as name, 
				post_id, content, image_path, 
				posts.created as created, users.user_type 
		FROM posts JOIN users 
		ON posts.user_id = users.user_id 
		WHERE users.status = 'A'
		ORDER BY posts.created DESC";

		if(!$stmt = $mysqli->prepare($selectUser))
			error_log("miniFacebook: " . "Post get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post get Failed : stmt->execute:" . $stmt->error) ;
			return $response;
		}

		if(!$stmt->bind_result($user_id, $name, $post_id, $content, $image_path, $created, $user_type)) {
			error_log("miniFacebook: " . "Post get Failed : stmt->bind result:" . $stmt->error) ;
			return $response;
		}

		while($stmt->fetch()) {
			$comments = array();
			array_push($response, 
				array(
					"user_id" 		=> htmlentities($user_id),
					"name"			=> htmlentities($name),
					"post_id"		=> htmlentities($post_id),
					"content" 		=> htmlentities($content),
					"image_path"	=> htmlentities($image_path),
					"created"		=> htmlentities($created),
					"user_type"		=> htmlentities($user_type)
				)
			);

		}

		foreach ($response as $key => $value) {
			$comments = getComments($value['post_id']);
			$response[$key]['comments'] = $comments;
		}		
		return $response;
	}

	function getComments($post_id) {

		global $mysqli;

		$response = array();

		$getCommentsSql = "SELECT comments.comment_id as commentId, users.user_id as user_id, users.name as name, post_id, comment_text, comments.created as created, users.user_type 
		FROM comments 
		JOIN users 
		ON comments.user_id = users.user_id 
		WHERE users.status = 'A' AND comments.post_id = ? ORDER BY comments.created DESC";

		if(!$stmt = $mysqli->prepare($getCommentsSql))
			error_log("miniFacebook: " . "Comment get Prepared Statement Error!" . $stmt->error );

		$stmt->bind_param("i", $post_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Comment get Failed : stmt->execute:" . $stmt->error) ;
			return $response;
		}

		if(!$stmt->bind_result($commentId, $user_id, $name, $post_id, $comment, $created, $user_type)) {
			error_log("miniFacebook: " . "Comment get Failed : stmt->bind result:" . $stmt->error) ;
			return $response;
		}

		while($stmt->fetch()) {
			array_push($response, 
				array(
					"user_id" 		=> htmlentities($user_id),
					"name"			=> htmlentities($name),
					"post_id"		=> htmlentities($post_id),
					"comment" 		=> htmlentities($comment),
					"created"		=> htmlentities($created),
					"comment_id"	=> htmlentities($commentId),
					"user_type" 	=> htmlentities($user_type)
				)
			);

		}
		return $response;
	}

	function getStats() {
		$stats = array();

		$userCount = getUserCount($stats);
		$postCount = getPostCount($stats);
		$commentCount = getCommentCount($stats);

		array_push($stats, 
    		array(
    			"count_user"	=> htmlentities($userCount),
    			"count_post"	=> htmlentities($postCount),
    			"count_comment" => htmlentities($commentCount)
    		)
    	);

		return $stats;

	}

	function getUserCount($stats) {
		global $mysqli;
		$countUser = "SELECT COUNT(user_id) as count_user FROM users WHERE status='A'";
		if(!$stmt = $mysqli->prepare($countUser))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_user)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_user);
		} 
	}

	function getPostCount($stats) {
		global $mysqli;
		$countPost = "SELECT COUNT(post_id) as count_post FROM posts JOIN users ON posts.user_id=users.user_id WHERE users.status = 'A'";
		if(!$stmt = $mysqli->prepare($countPost))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_post)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_post);
		} 
	}

	function getCommentCount($stats) {
		global $mysqli;
		$countPost = "SELECT COUNT(comment_id) as count_comment FROM comments JOIN users ON comments.user_id = users.user_id WHERE users.status = 'A'";
		if(!$stmt = $mysqli->prepare($countPost))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_comment)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_comment);
		} 
	}

	function insertPost($data) {
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$content_text   = htmlspecialchars($data["content"]);
		$image_path 	= htmlspecialchars($data["image_path"]);
		$status 		= "A";

		global $mysqli;

		$insertPost = "INSERT INTO posts(user_id, content, image_path, status) VALUES (?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertPost))
			error_log("miniFacebook: " . "Post insert Prepared Statement Error!");

		$stmt->bind_param("isss", $user_id, $content_text, $image_path, $status);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function savePost($data) { // Only owner can edit
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$content_text   = htmlspecialchars($data["content"]);
		$postid 		= htmlspecialchars($data['postid']);

		global $mysqli;

		$savePost = "UPDATE posts set content=? where user_id =? and post_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("sii", $content_text, $user_id, $postid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;
	}

	function deletePost($post_id) { // Change Status as Deleted, Only owner and superuser delete

		$user_id	 	= $_SESSION['user_id'];
		global $mysqli;

		$savePost = "DELETE from posts WHERE post_id = ? AND user_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("ii", $post_id, $user_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;
	}

	function addLike($id) {

	}

	function removeLike($id, $userid) {

	}

?>