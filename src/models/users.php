<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'config/db.php');

	function registerUser($registerData) {

		$name 		 = htmlspecialchars($registerData["name"]);

		$nickname    = htmlspecialchars($registerData["nickname"]);
		$email 		 = htmlspecialchars($registerData["email"]);
		$contact 	 = htmlspecialchars($registerData["contact"]);
		$location 	 = htmlspecialchars($registerData["location"]);
		$dob 		 = htmlspecialchars($registerData["dob"]);
		$gender 	 = htmlspecialchars($registerData["gender"]);
		$usertype 	 = htmlspecialchars($registerData["user_type"]);
		$password 	 = md5($registerData["password"]);

		$dob = empty($dob) ? NULL : $dob;

		global $mysqli;

		$insertUser = "INSERT INTO users(name, nickname, email, contact, location, dob, gender, user_type, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertUser))
			error_log("miniFacebook: " . "Register Prepared Statement Error!");

		$stmt->bind_param("sssisssss", $name, $nickname, $email, $contact, $location, $dob, $gender, $usertype, $password);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function login($data) {
		error_log(json_encode($data));
		$emailID = $data['email'];
		$password = md5($data['password']);

		global $mysqli;

		$selectUser = "SELECT user_type, name, email, user_id, status FROM users WHERE email = ? AND password=?";

		if(!$stmt = $mysqli->prepare($selectUser))
			error_log("miniFacebook: " . "Login Prepared Statement Error!");

		$stmt->bind_param("ss", $emailID, $password);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Login Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		$stmt->bind_result($user_type, $name, $email, $userid, $status);

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Login Failed " ."Store_result Error");
			return false;
		}
		error_log("Vrunda" . $stmt->num_rows);
		if($stmt->num_rows == 1) {
			error_log("Vrunda" . $user_type);
			if($stmt->fetch())  {
            	return array(
            		'userType'	=> htmlentities($user_type), 
            		'name'		=> htmlentities($name), 
            		'email'		=> htmlentities($email), 
            		'id'		=> htmlentities($userid), 
            		'status'	=> htmlentities($status)
            		);
			}
		}
		return false;
		
	}

	function changePasswordDB($user_id, $newpassword) {
		global $mysqli;
		$preparedSql = "UPDATE users SET password=md5(?) WHERE user_id = ?;";

		if(!$stmt = $mysqli->prepare($preparedSql))
			echo "Prepared statement error";

		$stmt->bind_param("si", $newpassword, $user_id);

		if(!$stmt->execute())  {
			echo "Execute Error";
			return false;
		}
		if(!$stmt->store_result()) {
			echo "Store result Error";
			return false;
		}
		return true;
	}

	function getUsers() {

		global $mysqli;

		$getUsers = "SELECT user_type, name, email, user_id, status, created FROM users";

		if(!$stmt = $mysqli->prepare($getUsers))
			error_log("miniFacebook: " . "Login Prepared Statement Error!");

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "get users Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		$stmt->bind_result($user_type, $name, $email, $userid, $status, $created);
		$response = array();
		while($stmt->fetch()) {
			array_push($response, 
				array(
				"user_id" 	  => htmlentities($userid),
				"name"		  => htmlentities($name),
				"email"		  => htmlentities($email),
				"user_type"   => htmlentities($user_type),
				"status"      => htmlentities($status),
				"created"	  => htmlentities($created)
			)
			);
		}
		// echo "<pre>";	
		// print_r($response);die();
		return $response;
	}

	function operation($user_id, $operation) {
		global $mysqli;

		$preparedSql = "UPDATE users SET status=? WHERE user_id = ?;";

		if(!$stmt = $mysqli->prepare($preparedSql))
			echo "Prepared statement error";

		$stmt->bind_param("si", $operation, $user_id);

		if(!$stmt->execute())  {
			echo "Execute Error";
			return false;
		}
		if(!$stmt->store_result()) {
			echo "Store result Error";
			return false;
		}
		return true;
	}
?>