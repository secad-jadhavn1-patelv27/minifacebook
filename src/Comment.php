<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'authenticate_session.php');

	require('models/comments.php');

	// print_r($_POST);
	if (isset($_POST) && !empty($_POST)) {
		switch ($_POST["form_type"]) {
			case 'Comment':
				addPostComment($_POST);
				break;

			case 'Edit Comment':
				editPost($_POST);
				break;
			
			default:
				echo "<script>alert('Invalid!');</script>";
				break;
		}
	}

	if (isset($_GET) && !empty($_GET)) {
		// print_r($_GET);
		// die();
		$csrfToken = $_GET['csrfTokenCommentDelete'];
		$commentId = $_GET['comment_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenCommentDelete'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}
		if(validateCommentId($commentId)) {
			//check if user is post owneror comment owner
			$owner = getOwnerType($commentId);
			print_R($owner);
			if(!empty($owner)) {
			 	$response = deleteComment($commentId);
			 	if($response != false) {
					// echo "<script>alert('Post successfully Deleted!');</script>";
			        header("Refresh:0; url=Views/profile.php");
			        die();
				}
			}
		}
		echo "<script>alert('Comment delete failed! Try again');</script>";
        header("Refresh:0; url=Views/profile.php");
        die();
	}

	function validateCommentId($commentId) {

		if(empty($commentId)) return FALSE;

		if(!is_numeric($commentId)) return FALSE;

		if ($commentId == 0) return FALSE;

		return TRUE;
	}

	function addPostComment($data) {
		// print_r($data); die();
		$csrfToken = $data['csrfTokenCommentCreate'];
		$postId = $data['post_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenCommentCreate'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}

		$res = addComment($data);
		if($res != false) {
			// echo "<script>alert('Post successfully saved!');</script>";
	        header("Refresh:0; url=Views/profile.php");
	        die();
		}
		echo "<script>alert('Failed to Comment!');</script>";
	        header("Refresh:0; url=Views/profile.php");
	        die();
	}

	function getComments($post_id){}
?>