<?php
	$domain = '/var/www/html/team3/src/';

	require($domain. 'session_create.php');

	if(!isset($_SESSION['logged']) or $_SESSION['logged'] != TRUE) {
		echo "<script>alert('Login to access!');</script>";
		session_destroy();
        header("Refresh:0; url=../login.php");
        die();
	}

	if($_SESSION['browser'] != $_SERVER['HTTP_USER_AGENT']) {
		echo "<script>alert('Session hijacking detected');</script>";
		session_destroy();
        header("Refresh:0; url=../Views/login.php");
        die();
	}
	// If not superuser redirect to the profile page
	if($_SESSION['role'] != 'S') { // s for superuser and u for user
		echo "<script>alert('Permission Denied! You do not have access to this page.');</script>";
        header("Refresh:0; url=../profile.php");
        die();
	}

?>