<?php
	$domain = '/var/www/html/team3/src/';

	require($domain.'admin/authenticate_session.php');

	require($domain.'models/users.php');

	require($domain.'models/posts.php');

	if (isset($_POST) && !empty($_POST)) {
		//print_r($_POST); die();
		addSuperuser($_POST);
	}

	if (isset($_GET) && !empty($_GET)) {
		$csrfToken = $_GET['csrfTokenSuperuser'];
		$user_id = $_GET['user_id'];
		$operation = $_GET['operation'];

		if($operation != 'D' && $operation != 'E') {
			echo "<script>alert('Invalid operation input!');</script>";
			die();
		}
		$operation = $operation == 'D'?'I':'A';
		if(!isset($csrfToken) or $_SESSION['csrfTokenSuperuser'] != $csrfToken){
			echo "<script>alert('CSRF attack detected!');</script>";
			die();
		}
		if(validateUserId($user_id)) {
		 	$response = operation($user_id, $operation);
		 	if($response != false) {
		        header("Refresh:0; url=../Views/admin/list.php");
		        die();
			}
		}
	}

	function validateUserId($postId) {

		if(empty($postId)) return FALSE;

		if(!is_numeric($postId)) return FALSE;

		if ($postId == 0) return FALSE;

		return TRUE;
	}

	function getUsersList() {
		return getUsers();
	}

	function getStatistics() {
		return getStats();
	}

	function addSuperuser($data) {
		// print_r($data); die();
		$registerData = $data;
		$email 		  = $data["email"];
		$contact 	  = $data["contact"];
		$password 	  = $data["password"];
		$conpassword  = $data["repassword"];

		unset($data["nickname"], $data["contact"], $data["location"], $data["gender"], $data["dob"]);

		if (validateInput($data) && 
			validatePassword($password, $conpassword) &&
			validateEmail($email) 
		) {
			if (!empty($contact) && !validateContact($contact)) {
				error_log("miniFacebook: " . "Invalid Contact!");
				return;
			}
			error_log("miniFacebook: " . "Successfully Validated!" . $data['name']) ;
			$registerData['user_type'] = 'S';
			if(registerUser($registerData)) {
				header("Location:../Views/admin/list.php");
				die();
			} else {
				echo '<script>alert("Registration unsuccessful. Try again!")</script>';
				header("Location:../Views/admin/list.php");
				die();
			}
		}
	}

	function validatePassword($password, $conpassword) {
		if ($password != $conpassword) {
			return FALSE;
		}
		if (strlen($password) < 8 || strlen($password) > 16) {
			return FALSE;
		}
		$pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$/";
		if(!preg_match($pattern, $password)) {
			return FALSE;
		}
		return TRUE;
	}

	function validateEmail($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  			$emailErr = "Invalid email format";
  			error_log("miniFacebook: " . $emailErr  ." : Email" . $email) ;
  			return FALSE;
		}
		return TRUE;
	}

	function validateInput($data) {
		foreach($data as $key => $input) {
			if (empty($input)) {
				error_log("miniFacebook: " . "$key field is empty!");
				return FALSE;
			}
			if ($key != 'gender' && $key != 'user_type' && strlen($input) < 5 && strlen($input) > 50) {
				error_log($key.' Invalid Input!');
				return FALSE;
			}
		} 
		return TRUE;
	}

	function validateContact($contact) {
		if (strlen($contact) != 10) {
			return FALSE;
		}
		if (!preg_match("/^(\.\d{2,4})?\s?(\d{10})$/", $contact)) {
			return FALSE;
		}
		return TRUE;
	}


?>