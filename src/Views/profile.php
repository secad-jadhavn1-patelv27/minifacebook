<?php
    require('../authenticate_session.php');

    require('../Post.php');

    /* Create Post CSRF token */
    $createRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostCreate"] = $createRand;

    /* Edit Post CSRF token */
    $editRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostEdit"] = $editRand;

    /* Delete Post CSRF token */
    $deleteRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostDelete"] = $deleteRand;

    /* Create Comment CSRF token */
    $commRandCreate = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenCommentCreate"] = $commRandCreate;

    /* Delete Comment CSRF token */
    $commRandDelete = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenCommentDelete"] = $commRandDelete;

    $response  = getPostsData();

    $statistics = getStatistics(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='admin/list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Activity Feed</h3>
            </div>

            <div class="panel-content panel-activity">
                <!-- Add Post -->
                <form method="POST" action="../Post.php" class="panel-activity__status">
                    <input type="hidden" name="csrfTokenPostCreate" value="<?php echo $createRand; ?>">
                    <textarea name="content" placeholder="Share what you've been up to..." class="form-control" maxlength="200" required></textarea>
                    <input type="hidden" name="image_path">
                    <div class="actions">
                        <div class="btn-group">
                            
                        </div>
                        <button type="submit" class="btn btn-rounded btn-info" name="form_type" value="Post">
                            Post
                        </button>
                    </div>
                </form> <!-- End of Add Post -->

                <!-- Post List -->
                <ul class="panel-activity__list">
                <?php
                if(!empty($response)) {
                    
                    foreach ($response as $key => $value) {
                ?>
                <li>
                    <!-- Post Owner name and Action -->
                    <div class="activity__list__header">

                        <img src="postProfile.png" style="max-width: 40px;" alt="" />

                        <a href="#">
                            <?php 
                                echo "<span style=font-size:20px>".$value['name']."</span>"; 
                                if($value['user_type'] == 'S') {
                                    echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:15px' title='Superuser'></i>";
                                }
                            ?> 
                        </a>

                        <!-- Edit and Delete Options only to Owner -->
                        <?php
                            if($_SESSION['user_id'] == $value['user_id']) {
                        ?>
                            <!-- Edit Post Link--> 
                            <i class="fa fa-pencil editPostClass" id="postid_<?php echo $value['post_id']; ?>"></i>
                            <!-- Delete Post Link --> 
                            <a href="../Post.php?post_id=<?php echo $value['post_id']; ?>&csrfTokenPostDelete=<?php echo $deleteRand?> "><i class="fa fa-trash"></i></a>
                        <?php } ?>

                        <br>
                        <div style="color: black;" id="post_<?php echo $value['post_id']; ?>"><?php echo $value['content'];?>
                            <span style="float:right">
                                <?php echo date('d M Y', strtotime($value['created'])); ?>
                            </span>
                        </div>

                    </div> <!-- End of Post Owner and Action -->

                    <div class="activity__list__body entry-content">
                        <p>
                            <!-- Display the post content -->
                            <!-- <div id="post_<?php //echo $value['post_id']; ?>"><?php //echo $value['content'];?></div> -->

                            <!-- Edit post for owners only -->
                            <?php
                                if($_SESSION['user_id'] == $value['user_id']) {
                            ?>
                            <div id="edit_postid_<?php echo $value['post_id']; ?>" class="editPost">
                                <form method="POST" action="../Post.php" class="panel-activity__status">
                                    <input type="hidden" name="csrfTokenPostEdit" value="<?php echo $editRand; ?>">
                                    <input type="hidden" name="postid" value="<?php echo $value['post_id']; ?>">
                                    <input name="content" maxlength="200" required class="form-control" value="<?php echo $value['content']; ?>">
                                    <div class="actions">
                                        <button type="submit" class="btn btn-sm btn-rounded btn-info" name="form_type" value="Edit Post">Save</button>
                                    </div>
                                </form>
                                <br></br>
                            </div>
                            <?php
                                }
                            ?>
                            <!-- End of Edit post for owners only -->
                        </p>
                    </div>

                    <!-- List Comments -->
                    <div class="panel-activity__status">
                        <?php 
                            if(!empty($value['comments'])){
                        ?>
                                <ul class="actions panel-activity__list comment_list">
                        <?php
                                foreach ($value['comments'] as $comm) {
                        ?>
                                <li>
                                    <?php 
                                        echo "<b>".$comm['name'] ."</b>"; 
                                        if($comm['user_type'] == 'S') {
                                            echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:15px' title='Superuser'></i>";
                                        }

                                    ?>
                                    <?php
                                        if($_SESSION['user_id'] == $comm['user_id'] || $_SESSION['user_id'] == $value['user_id']) {
                                    ?> 
                                        <a style="color:black" href="../Comment.php?comment_id=<?php echo $comm['comment_id']; ?>&csrfTokenCommentDelete=<?php echo $commRandDelete?> "><i class="fa fa-trash"></i></a>
                                    <?php } ?>

                                    <?php 
                                    echo "<br><span style='padding-left:2%'>" . $comm['comment']."</span>"; ?>
                                    <span style="float:right">
                                    <?php echo date('d M Y', strtotime($comm['created'])); ?>
                                    </span>
                                </li>
                                <hr>
                                
                        <?php      }?>
                                </ul>
                        <?php  } ?>
                           
                    </div> <!-- End of List Comments -->

                    <!-- Add Comments -->
                    <form method="POST" action="../Comment.php" class="panel-activity__status">
                        <input type="hidden" name="csrfTokenCommentCreate" value="<?php echo $commRandCreate; ?>">
                        <input type="text" name="comment" placeholder="Share your comments..." class="form-control" required maxlength="200">
                        <input type="hidden" name="post_id" value="<?php echo $value['post_id']; ?>">
                        <div class="actions">
                
                            <button type="submit" class="btn btn-sm btn-rounded btn-info" name="form_type" value="Comment">
                                Comment
                            </button>
                        </div>
                    </form> <!-- End of Add Comments -->

                </li>
                <?php
                    }
                } else {
                    echo "No Posts";
                }
                ?>

                </ul> <!-- Post List -->

            </div>

        </div> <!-- End of Activity Feed -->
    </div>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.editPost').hide();

            $( ".editPostClass" ).click(function() {
                var postid = this.id.split("_")[1];
                // alert("show" + "#edit_postid_"+postid);
                $("#edit_postid_"+postid).show();
                $("#post_"+postid).hide();

            });

        });
    </script>
</body>
</html>