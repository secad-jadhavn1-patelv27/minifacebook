<?php
   $domain = '/var/www/html/team3/src/';

   require($domain.'admin/authenticate_session.php');
   require('../../Post.php');
   $statistics = getStatistics();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "../style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="../profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            
            <div class="panel-content panel-activity">
                <div class="panel-activity_status" align="center">
                    <div class="heading regForm">Register Superuser</div><br>
         
                   <form name="dform" method="POST" onsubmit="return validate()" action="../../admin/User.php">
                          <table class="regForm" style="font-size: 18px">
                             <tr>
                                <th>Name <span class="star">*</span><br><input type="text" name="name" class="input" maxlength="30" required></th>
                                <th>Nickname <br><input type="text" name="nickname" maxlength="15" class="input"></th>
                             </tr>
                             <tr>
                                <th>Email <span class="star">*</span><br><input type="email" name="email" id="myEmail"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="input" required></th>
                                <th>Contact <br><input type="text" name="contact" id="mycont" class="input" pattern="^(\+\d{2,4})?\s?(\d{10})$"></th>
                             </tr>
                             <tr>
                                <th rowspan="2">Location <br><textarea type="text" name="location" class="input" maxlength="50"></textarea></th>
                                <th>Date Of Birth <span class="star">*</span><br><input type="date" name="dob" class="input" required></th>
                             </tr>   
                             <tr>
                                <th>
                                   Gender <span class="star">*</span><br>
                                   <select id="gn" name="gender" required>
                                      <option value=""></option>
                                      <option value="F">Female</option>
                                      <option value="M">Male</option>
                                      <option value="O">Other</option>
                                </th>
                             </tr>
                             <tr>
                                <th>Password <span class="star">*</span><br><input type="password" name="password" class="input" minlength="8" maxlength="16" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$" title="Password must have at least 8 and maximum 16 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE"  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
                                form.repassword.pattern = this.value;"></th>
                                <th>Confirm Password <span class="star">*</span><br><input type="password" name="repassword" class="input" minlength="8" maxlength="16" required  required title="Password does not match" onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"></th>
                             </tr>
                             <tr>
                                <th><br><span class="star">* Mandatory Fields</span><br></br>
                                <th><br>Already signed up?  <a href="login.php">Sign In!</a><br></br>
                             </tr>
                             <tr class="subbtn">
                                <th  colspan="2"><input class="button" type="submit" name="form_type" value="Register"></th>
                             </tr>
                          </table>
                   </form>
                </div>
            </div>

        </div> <!-- End of Activity Feed -->
    </div>
    
</body>
</html>