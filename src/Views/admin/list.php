<?php
    $domain = '/var/www/html/team3/src/';
    require($domain.'admin/authenticate_session.php');

    require($domain.'admin/User.php');

    /* Disable User CSRF token */
    $csrfTokenSuperuser = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenSuperuser"] = $csrfTokenSuperuser;


    $statistics = getStatistics(); 
    $response  = getUsersList();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "../style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        table {
            color: black;
            border: 1px solid black;
        }
        td, thead {
            text-align: center;
        }
        .bg {
            background-color: #d0a9ec80;
        }
        .rowbg {
            background-color: white;
        }
    </style>
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="../profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'] . " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../profile.php';"">
                    <i class="fa fa-user"></i>
                    <span>Profile</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Users List -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Users List</h3>
                <!-- <button class="btn btn-rounded btn-info" style="float:right;" onclick="document.location.href='superuser_register.php';">
                    <i class="fa fa-user-plus"></i>
                    <span>Add Superuser</span>
                </button> -->
            </div>

            <div class="panel-content panel-activity">
                <table class="table table-bordered table-hover">
                    <?php if(!empty($response)) {?>
                    <thead class="bg">
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        <?php foreach($response as $user) {
                            if($user['user_type'] == 'U') {?>
                        <tr class="rowbg">
                            <td><?php echo $user['name'];?> </td>
                            <td><?php echo $user['email'];?></td>
                            <td><?php echo date('d M Y, h:i A', strtotime($user['created']));?></td>
                            <td>
                                <!-- If enabled... add button disable -->
                                <?php if($user['status'] == 'A') {?>
                                    <a href="../../admin/User.php?user_id=<?php echo $user['user_id']; ?>&csrfTokenSuperuser=<?php echo $csrfTokenSuperuser?>&operation=D">
                                        <button class="btn btn-rounded btn-danger">
                                            Disable User
                                        </button>
                                    </a>
                                <?php 
                                    }
                                    else {
                                ?>
                                <!-- If disable... add button enable -->
                                    <a href="../../admin/User.php?user_id=<?php echo $user['user_id']; ?>&csrfTokenSuperuser=<?php echo $csrfTokenSuperuser?>&operation=E">
                                        <button class="btn btn-rounded btn-success">
                                            Enable User
                                        </button>
                                <?php } }?>

                            </td>
                        </tr>
                    <?php 
                        } // end of for loop to display users
                    ?>
                    </tbody>
                    <?php 
                        } // end of  empty response
                        else {
                            echo "No Users!";
                        } 
                    ?>
                </table>
            </div>

        </div> <!-- End of Users List -->

        <!-- Superusers List -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Superusers List</h3>
                <button class="btn btn-rounded btn-info" style="float:right;" onclick="document.location.href='superuser_register.php';">
                    <i class="fa fa-user-plus"></i>
                    <span>Add Superuser</span>
                </button>
            </div>

            <div class="panel-content panel-activity">
                <table class="table table-bordered table-hover">
                    <?php if(!empty($response)) {?>
                    <thead class="bg">
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Created</th>
                    </thead>
                    <tbody>
                        <?php foreach($response as $user) {
                            if($user['user_type'] == 'S') { ?>
                        <tr class="rowbg">
                            <td><?php echo $user['name'];?> </td>
                            <td><?php echo $user['email'];?></td>
                            <td><?php echo date('d M Y, h:i A', strtotime($user['created']));?></td>
                        </tr>
                    <?php 
                            } 
                        }// end of for loop to display superusers
                    ?>
                    </tbody>
                    <?php 
                        } // end of  empty response
                        else {
                            echo "No Users!";
                        } 
                    ?>
                </table>
            </div>

        </div> <!-- End of Superuser List -->
    </div>
    <script type="text/javascript">
    </script>
</body>
</html>