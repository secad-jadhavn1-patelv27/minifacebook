<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'authenticate_session.php');

	require($domain.'models/posts.php');

	if (isset($_POST) && !empty($_POST)) {
		switch ($_POST["form_type"]) {
			case 'Post':
				addPost($_POST);
				break;

			case 'Edit Post':
				editPost($_POST);
				break;

			case '':
				if (validateInput($_POST)) { 
					changePasswordHandler($_POST);
				}
				break;
			
			default:
				echo "<script>alert('Invalid!');</script>";
				break;
		}
	}
	if (isset($_GET) && !empty($_GET)) {
		// print_r($_GET);
		// die();
		$csrfToken = $_GET['csrfTokenPostDelete'];
		$postId = $_GET['post_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostDelete'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}
		if(validatePostId($postId)) {
		 	$response = deletePost($postId);
		 	if($response != false) {
				// echo "<script>alert('Post successfully Deleted!');</script>";
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		}
	}

	function validateInput($data) {
		if (empty($data['content'])) {
			error_log("miniFacebook: " . " post content is empty!");
			return FALSE;
		}
		return TRUE;
	}

	function validatePostId($postId) {

		if(empty($postId)) return FALSE;

		if(!is_numeric($postId)) return FALSE;

		if ($postId == 0) return FALSE;

		return TRUE;
	}

	function editPost($data) {
		$csrfToken = $data['csrfTokenPostEdit'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostEdit'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		 }
		if(validateInput($_POST) && !empty($_POST['postid'])) {
			$res = savePost($data); 
			if($res != false) {
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		} else{
			echo "<script>alert('Invalid post content');</script>";
			header("Location:Views/profile.php");
			die();
		}
		//echo "<script>alert('Post save failed!');</script>";
		header("Location:Views/prrofile.php");
	}

	function addPost($data) {
		$csrfToken = $data['csrfTokenPostCreate'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostCreate'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		 }
		if(validateInput($_POST)) {
			$res = insertPost($data); 
			if($res != false) {
				// echo "<script>alert('Post successfully saved!');</script>";
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		} else{
			echo "<script>alert('Invalid post content');</script>";
			header("Location:Views/profile.php");
			die();
		}
		echo "<script>alert('Post save failed!');</script>";
		header("Location:Views/prrofile.php");
	}

	function getPostsData() {
		return getAllPosts();
	}

	function getStatistics() {
		return getStats();
	}

?>