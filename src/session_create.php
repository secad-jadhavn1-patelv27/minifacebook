<?php
	$LIFETIME 	= 15*60; // 15 minutes
	$PATH 		= "/";
	$DOMAIN 	= ".minifacebook.com";
	$SECURE 	= TRUE;
	$HTTPONLY 	= TRUE;
	
	session_set_cookie_params($LIFETIME, $PATH, $DOMAIN, $SECURE, $HTTPONLY);

	session_start();
?>