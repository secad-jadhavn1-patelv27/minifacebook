## Team Project ##

University of Dayton

Department of Computer Science

CPS 575 Secure Application Development

Instructor Dr. Phu Phung  
&nbsp; 

# **MiniFacebook** #

&nbsp; 
## Bitbucket Repository Link ##
[https://bitbucket.org/secad-jadhavn1-patelv27/minifacebook/src/master/](https://bitbucket.org/secad-jadhavn1-patelv27/minifacebook/src/master/)

## Team 3 - Members: ##
* Member 1
	- Name: Nirmala Jadhav 
	- StudentID:101679834
	- Email: jadhavn1@udayton.edu
* Member 2
	- Name:  Vrunda Patel
	- StudentID:101694589
	- Email: patelv27@udayton.edu  

&nbsp;   	
## **INTRODUCTION** ##

## Description ##
Developed secure web application that mimic social media website which allows users with age 13+ to create account and login to their account. Logged in users can 
* View posts added by all the users
* Share,edit and delete their own posts
* Add and delete comment 
* Post owner can delete comments on their post
* Live chat with other logged in users

## Project Design ##
The project is developed on the Model View Controller module where 
* Model is responsible for all database related functionalities
* Views comprises of User interface of this web application
* Controller is a bridge between Views and Models where data is processed and transfered 

## Development ##
The web application is developed on following technologies:
* Server       - Apache, NodeJS
* Server side  - PHP, NodeJS
* Database	   - MySQL
* Client side  - HTML, CSS, JavaScript 

## Achievement ##
* User with eligible age[13+] can register 
* Already registered active users can login to the web application
* Registered users can
	- View/Add/Edit/Delete a post
	- View/Add/Delete a comment
	- Change password
* Superusers can
	- Login
	- Create superuser
	- View list of registered users [regular &  superusers]
	- Enable/Disable regular users
* Logged in users can join a real-time public chat with other logged-in users
* Prevented web application from following attacks:
	- Session hijacking
	- SQL Injection
	- Cross site scripting (XSS)
	- Cross site request forgery (CSRF)
* Implemented input validation at all layers to achieve robust and defensive web application

## Bitbucket Repository Link ##
[https://bitbucket.org/secad-jadhavn1-patelv27/minifacebook/src/master/](https://bitbucket.org/secad-jadhavn1-patelv27/minifacebook/src/master/)

&nbsp; 
## **DESIGN** ##
* Database

	Designed relational database for web application as described in follwoing ER diagram 

	ER Diagram
	![alt text](report_images/ER_minifacebook.jpg)

* User Interface
	- Login Interface for both regular and super users
	![alt text](report_images/UI_login.png)

	- Registration page to create new account for regular users
	![alt text](report_images/UI_register.png)

	- A header of activity feed page with active user's statistics and navigation to other pages 
	![alt text](report_images/UI_profileHeader.png)

	- Activity feed page which displays all the posts and comments and provides provision to share a new post 
	![alt text](report_images/UI_post_and_comments.png)

	- Real-time public chat interface
	![alt text](report_images/UI_chat.png)

	- Regular users list accessed by super user  
	![alt text](report_images/UI_userslist.png)

	- Superuserlist and register superuser
	![alt text](report_images/UI_SuperUserReg.png)

	- Change password for regular user
	![alt text](report_images/11_changepassword_user.png)

	- Change password for superuser
	![alt text](report_images/10_changepassword_superuser.png)

* Functionalities of application
	- Separated the privileges between regular and super users by using 'role' parameter in session($_SESSION['role']) and 'user_type' column in users table 
	- The privilege is managed among the code by keeping separate admin folder for superuser accessible pages 
	- Integrated NodeJS chat application with one of the php file to provide public live chat functionality to logged in users
&nbsp; 
&nbsp;
## **IMPLEMENTATION AND SECURITY ANALYSIS** ##
* Security features such as compartmentalization as well as least privilege for users were implemented using MVC design based on their roles.

* To achieve defense-in-depth, We included input validation at all layers i.e. User Interface, Database and at server-side.

* Validated for absurd and unpredictable user input to achieve robustness in the web application.

* Considered security measures at every stage of development life cycle in order to accomplish defense in breadth principle.

* Used new database user which has limited access to only application's database i.e. minifacebook. Added foreign key constraint to maintain consistency in database. Saved hash value for sensitive data such as password.

* Prevented session-hijacking by using 
	- HTTPS setup for web application
	- session_set_cookie_params($lifetime, $path, $domain, $secure, $http-only)
	where $secure => TRUE, transfer cookie value via HTTPS to prevent man-in-middle attack
	$http-only => TRUE, JavaScript can not read/write this cookie value to prevent XSS 
	- checking one more parameter ['HTTP_USER_AGENT'] to confirm the identity of the browser 
 
*  Avoided XSS by sanitizing input and output using htmlspecialchars() and htmlentities() 

* Used prepare statement for SQL queries in order to prevent SQL injection attack

* CSRF attack is prevented with secure session token in all forms except register and login.

* 'role' parameter in session($ SESSION['role']) and 'user type' column in users table were used to distinguish between regular and super users. The privilege is controlled by keeping a separate admin folder for superuser-accessible webpages in the code.

&nbsp; 
## **DEMO** ##
* Registration page for regular users
![alt text](report_images/2_register.png)

* Login Page for both regular and super users
![alt text](report_images/1_login.png)

* Superuser specific access
	- Disabled user can not login
	![alt text](report_images/8.1_disableUserLoginFailed.png)

	- Superuser can enable the disabled regular user
	![alt text](report_images/8_enableUser.png)

	- Enabled regular user Sunny can now login 
	![alt text](report_images/enableUserLogin.png)

	- Superusers list with add superuser button  
	![alt text](report_images/7_superuser_superuser_list.png)


* Functionalities on Post
	- Share a post
	![alt text](report_images/3_create_post.png)

	- Edit a post
	![alt text](report_images/3.1_edit_post.png)

	- Activity feed after user sunny deleted the existing post "Hello, My First Post!" 
	![alt text](report_images/3.1_delete_post.png)

	- Prevent CSRF attack on delete post
	![alt text](report_images/3.2_deletePostCSRFPrevention.png)

* Access control to distinguish regular and super users permissions[user list and superuser registration are not accessible to regular user]
![alt text](report_images/12_regularUserCanNotAccess.png)

* Live Public Chat between Sakshi and Niketan
	![alt text](report_images/sakshiChat.png)

	![alt text](report_images/niketanTyping.png)

&nbsp; 
## **APPENDIX** ##
### Source code###
## database.sql
```sql
-- Database and User creation 

-- CREATE database miniFacebook;

-- create user 'minifb'@'localhost' IDENTIFIED BY 'team3';

-- grant all on miniFacebook.* to 'minifb'@'localhost';

-- If table exists, delete it.
DROP TABLE IF EXISTS `users`;

-- Create a new table 'users'.
CREATE TABLE users(
	user_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name varchar(50) NOT NULL,
	nickname varchar(50),
	email varchar(50) NOT NULL UNIQUE,
	contact varchar(10), 
	location varchar(50),
	dob date NOT NULL,
	gender enum('F','M','O') NOT NULL,
	user_type enum('S', 'U') DEFAULT 'U',
	password varchar(50) NOT NULL,
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
 
-- If table exists, delete it.
DROP TABLE IF EXISTS `posts`;

-- Create a new table 'users'.
CREATE TABLE posts(
	post_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_id int(11) NOT NULL,
	content text NOT NULL,
	image_path varchar(100),
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE
);

-- If table exists, delete it.
DROP TABLE IF EXISTS `comments`;

-- Create a new table 'users'.
CREATE TABLE comments(
	comment_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	post_id int(11) NOT NULL,
	user_id int(11) NOT NULL,
	comment_text text NOT NULL,
	status enum('A', 'I') DEFAULT 'A',
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id), 
	FOREIGN KEY (post_id) REFERENCES posts(post_id) ON DELETE CASCADE
);

-- If table exists, delete it.
DROP TABLE IF EXISTS `likes`;

-- Create a new table 'users'.
CREATE TABLE likes(
	like_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	post_id int(11) NOT NULL,
	user_id int(11) NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id)
);
```
## /models/comments.php
```php
<?php
	require('config/db.php');

	function addComment($data) {
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$post_id		= htmlspecialchars($data["post_id"]);
		$comment_text   = htmlspecialchars($data["comment"]);
		$status 		= htmlspecialchars("A");

		global $mysqli;

		$insertPost = "INSERT INTO comments(user_id, comment_text, post_id, status) VALUES (?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertPost))
			error_log("miniFacebook: " . "Post insert Prepared Statement Error!");

		$stmt->bind_param("isis", $user_id, $comment_text, $post_id, $status);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function editComment($commentid) { // Owner of comment, owner of post and superuser

	}

	function getOwnerType($commentid) {
		global $mysqli;
		$post_id= 0 ;

		$commentDetails = "SELECT user_id, post_id from comments WHERE comment_id = ?";

		if(!$stmt = $mysqli->prepare($commentDetails))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("i", $commentid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return "";
		}

		$stmt->bind_result($user_id, $post_id);

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return "";
		}
		error_log("Nirmala Comment Owner: ". $stmt->num_rows);

		if($stmt->num_rows == 1) {
			if($stmt->fetch())  {
				error_log("Nirmala Comment Owner: ". $stmt->num_rows ." ::: " . $user_id. "::". $post_id);
				if($_SESSION['user_id'] == $user_id)
					return "comment";
			}
		}

		if(isPostOwner($post_id)) {
			error_log("Nirmala Post Owner: ". $post_id);
			return "post";
		}

		return "";
	}

	function isPostOwner($post_id) {
		$user_id	 	= $_SESSION['user_id'];
		global $mysqli;

		$postDetails = "SELECT user_id from posts WHERE post_id = ? and user_id=?";

		if(!$stmt = $mysqli->prepare($postDetails))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("ii", $post_id, $user_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		if($stmt->num_rows == 1) {
			error_log("Vrunda Post owner");
			return true;
		}

		return false;
	}

	function deleteComment($commentid) { // Owner of comment, owner of post and superuser

		global $mysqli;

		$savePost = "DELETE from comments WHERE comment_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("i", $commentid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;

	}



?>
```
## /models/posts.php
```php
<?php
	require $domain.'config/db.php';

	function getAllPosts() { // All active posts 

		global $mysqli;

		$response = array();

		$selectUser = "SELECT 
				users.user_id as user_id, users.name as name, 
				post_id, content, image_path, 
				posts.created as created, users.user_type 
		FROM posts JOIN users 
		ON posts.user_id = users.user_id 
		WHERE users.status = 'A'
		ORDER BY posts.created DESC";

		if(!$stmt = $mysqli->prepare($selectUser))
			error_log("miniFacebook: " . "Post get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post get Failed : stmt->execute:" . $stmt->error) ;
			return $response;
		}

		if(!$stmt->bind_result($user_id, $name, $post_id, $content, $image_path, $created, $user_type)) {
			error_log("miniFacebook: " . "Post get Failed : stmt->bind result:" . $stmt->error) ;
			return $response;
		}

		while($stmt->fetch()) {
			$comments = array();
			array_push($response, 
				array(
					"user_id" 		=> htmlentities($user_id),
					"name"			=> htmlentities($name),
					"post_id"		=> htmlentities($post_id),
					"content" 		=> htmlentities($content),
					"image_path"	=> htmlentities($image_path),
					"created"		=> htmlentities($created),
					"user_type"		=> htmlentities($user_type)
				)
			);

		}

		foreach ($response as $key => $value) {
			$comments = getComments($value['post_id']);
			$response[$key]['comments'] = $comments;
		}		
		return $response;
	}

	function getComments($post_id) {

		global $mysqli;

		$response = array();

		$getCommentsSql = "SELECT comments.comment_id as commentId, users.user_id as user_id, users.name as name, post_id, comment_text, comments.created as created, users.user_type 
		FROM comments 
		JOIN users 
		ON comments.user_id = users.user_id 
		WHERE users.status = 'A' AND comments.post_id = ? ORDER BY comments.created DESC";

		if(!$stmt = $mysqli->prepare($getCommentsSql))
			error_log("miniFacebook: " . "Comment get Prepared Statement Error!" . $stmt->error );

		$stmt->bind_param("i", $post_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Comment get Failed : stmt->execute:" . $stmt->error) ;
			return $response;
		}

		if(!$stmt->bind_result($commentId, $user_id, $name, $post_id, $comment, $created, $user_type)) {
			error_log("miniFacebook: " . "Comment get Failed : stmt->bind result:" . $stmt->error) ;
			return $response;
		}

		while($stmt->fetch()) {
			array_push($response, 
				array(
					"user_id" 		=> htmlentities($user_id),
					"name"			=> htmlentities($name),
					"post_id"		=> htmlentities($post_id),
					"comment" 		=> htmlentities($comment),
					"created"		=> htmlentities($created),
					"comment_id"	=> htmlentities($commentId),
					"user_type" 	=> htmlentities($user_type)
				)
			);

		}
		return $response;
	}

	function getStats() {
		$stats = array();

		$userCount = getUserCount($stats);
		$postCount = getPostCount($stats);
		$commentCount = getCommentCount($stats);

		array_push($stats, 
    		array(
    			"count_user"	=> htmlentities($userCount),
    			"count_post"	=> htmlentities($postCount),
    			"count_comment" => htmlentities($commentCount)
    		)
    	);

		return $stats;

	}

	function getUserCount($stats) {
		global $mysqli;
		$countUser = "SELECT COUNT(user_id) as count_user FROM users WHERE status='A'";
		if(!$stmt = $mysqli->prepare($countUser))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_user)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_user);
		} 
	}

	function getPostCount($stats) {
		global $mysqli;
		$countPost = "SELECT COUNT(post_id) as count_post FROM posts JOIN users ON posts.user_id=users.user_id WHERE users.status = 'A'";
		if(!$stmt = $mysqli->prepare($countPost))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_post)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_post);
		} 
	}

	function getCommentCount($stats) {
		global $mysqli;
		$countPost = "SELECT COUNT(comment_id) as count_comment FROM comments JOIN users ON comments.user_id = users.user_id WHERE users.status = 'A'";
		if(!$stmt = $mysqli->prepare($countPost))
			error_log("miniFacebook: " . "Stats get Prepared Statement Error!" . $stmt->error );

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->execute:" . $stmt->error) ;
			return $stats;
		}

		if(!$stmt->bind_result($count_comment)) {
			error_log("miniFacebook: " . "Stats get Failed : stmt->bind result:" . $stmt->error) ;
			return $stats;
		}
		if($stmt->fetch())  {
			return htmlentities($count_comment);
		} 
	}

	function insertPost($data) {
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$content_text   = htmlspecialchars($data["content"]);
		$image_path 	= htmlspecialchars($data["image_path"]);
		$status 		= "A";

		global $mysqli;

		$insertPost = "INSERT INTO posts(user_id, content, image_path, status) VALUES (?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertPost))
			error_log("miniFacebook: " . "Post insert Prepared Statement Error!");

		$stmt->bind_param("isss", $user_id, $content_text, $image_path, $status);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function savePost($data) { // Only owner can edit
		$user_id	 	= htmlspecialchars($_SESSION['user_id']);
		$content_text   = htmlspecialchars($data["content"]);
		$postid 		= htmlspecialchars($data['postid']);

		global $mysqli;

		$savePost = "UPDATE posts set content=? where user_id =? and post_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("sii", $content_text, $user_id, $postid);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;
	}

	function deletePost($post_id) { // Change Status as Deleted, Only owner and superuser delete

		$user_id	 	= $_SESSION['user_id'];
		global $mysqli;

		$savePost = "DELETE from posts WHERE post_id = ? AND user_id = ?";

		if(!$stmt = $mysqli->prepare($savePost))
			error_log("miniFacebook: " . "Post edit Prepared Statement Error!");

		$stmt->bind_param("ii", $post_id, $user_id);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Post change failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Post change failed" ."Store_result Error");
			return false;
		}

		return true;
	}

	function addLike($id) {

	}

	function removeLike($id, $userid) {

	}

?>
```
## /models/users.php
```php
<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'config/db.php');

	function registerUser($registerData) {

		$name 		 = htmlspecialchars($registerData["name"]);

		$nickname    = htmlspecialchars($registerData["nickname"]);
		$email 		 = htmlspecialchars($registerData["email"]);
		$contact 	 = htmlspecialchars($registerData["contact"]);
		$location 	 = htmlspecialchars($registerData["location"]);
		$dob 		 = htmlspecialchars($registerData["dob"]);
		$gender 	 = htmlspecialchars($registerData["gender"]);
		$usertype 	 = htmlspecialchars($registerData["user_type"]);
		$password 	 = md5($registerData["password"]);

		$dob = empty($dob) ? NULL : $dob;

		global $mysqli;

		$insertUser = "INSERT INTO users(name, nickname, email, contact, location, dob, gender, user_type, password) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		if(!$stmt = $mysqli->prepare($insertUser))
			error_log("miniFacebook: " . "Register Prepared Statement Error!");

		$stmt->bind_param("sssisssss", $name, $nickname, $email, $contact, $location, $dob, $gender, $usertype, $password);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Registration Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Registration Failed " ."Store_result Error");
			return false;
		}

		return true;
	}

	function login($data) {
		error_log(json_encode($data));
		$emailID = $data['email'];
		$password = md5($data['password']);

		global $mysqli;

		$selectUser = "SELECT user_type, name, email, user_id, status FROM users WHERE email = ? AND password=?";

		if(!$stmt = $mysqli->prepare($selectUser))
			error_log("miniFacebook: " . "Login Prepared Statement Error!");

		$stmt->bind_param("ss", $emailID, $password);

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "Login Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		$stmt->bind_result($user_type, $name, $email, $userid, $status);

		if(!$stmt->store_result()) {
			error_log("miniFacebook: " . "Login Failed " ."Store_result Error");
			return false;
		}
		error_log("Vrunda" . $stmt->num_rows);
		if($stmt->num_rows == 1) {
			error_log("Vrunda" . $user_type);
			if($stmt->fetch())  {
            	return array(
            		'userType'	=> htmlentities($user_type), 
            		'name'		=> htmlentities($name), 
            		'email'		=> htmlentities($email), 
            		'id'		=> htmlentities($userid), 
            		'status'	=> htmlentities($status)
            		);
			}
		}
		return false;
		
	}

	function changePasswordDB($user_id, $newpassword) {
		global $mysqli;
		$preparedSql = "UPDATE users SET password=md5(?) WHERE user_id = ?;";

		if(!$stmt = $mysqli->prepare($preparedSql))
			echo "Prepared statement error";

		$stmt->bind_param("si", $newpassword, $user_id);

		if(!$stmt->execute())  {
			echo "Execute Error";
			return false;
		}
		if(!$stmt->store_result()) {
			echo "Store result Error";
			return false;
		}
		return true;
	}

	function getUsers() {

		global $mysqli;

		$getUsers = "SELECT user_type, name, email, user_id, status, created FROM users";

		if(!$stmt = $mysqli->prepare($getUsers))
			error_log("miniFacebook: " . "Login Prepared Statement Error!");

		if(!$stmt->execute()) {
			error_log("miniFacebook: " . "get users Failed : stmt->execute:" . $stmt->error) ;
			return false;
		}

		$stmt->bind_result($user_type, $name, $email, $userid, $status, $created);
		$response = array();
		while($stmt->fetch()) {
			array_push($response, 
				array(
				"user_id" 	  => htmlentities($userid),
				"name"		  => htmlentities($name),
				"email"		  => htmlentities($email),
				"user_type"   => htmlentities($user_type),
				"status"      => htmlentities($status),
				"created"	  => htmlentities($created)
			)
			);
		}
		// echo "<pre>";	
		// print_r($response);die();
		return $response;
	}

	function operation($user_id, $operation) {
		global $mysqli;

		$preparedSql = "UPDATE users SET status=? WHERE user_id = ?;";

		if(!$stmt = $mysqli->prepare($preparedSql))
			echo "Prepared statement error";

		$stmt->bind_param("si", $operation, $user_id);

		if(!$stmt->execute())  {
			echo "Execute Error";
			return false;
		}
		if(!$stmt->store_result()) {
			echo "Store result Error";
			return false;
		}
		return true;
	}
?>
```
## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>A Simple login form, SecAD</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>

</body>
</html>


```
## /config/db.php
```php
<?php
	require('constants.php');

	$mysqli = new mysqli($DB_SERVER, $DB_USER , $DB_PASSWORD, $DB_NAME);

	if($mysqli->connect_errno) {
		error_log("miniFacebook: " + "Database Connection Failed - " +  $mysqli->connect_error);
		exit();
	}  
?>
```
## /config/constants.php
```php
<?php
	$DB_SERVER 		= "localhost";
	$DB_USER 		= "minifb";
	$DB_PASSWORD 	= "team3";
	$DB_NAME		= "miniFacebook";
?>
```
## /session_create.php
```php
<?php
	$LIFETIME 	= 15*60; // 15 minutes
	$PATH 		= "/";
	$DOMAIN 	= ".minifacebook.com";
	$SECURE 	= TRUE;
	$HTTPONLY 	= TRUE;
	
	session_set_cookie_params($LIFETIME, $PATH, $DOMAIN, $SECURE, $HTTPONLY);

	session_start();
?>
```
## /authenticate_session.php
```php
<?php
	$domain = '/var/www/html/team3/src/';

	require($domain. 'session_create.php');

	if(!isset($_SESSION['logged']) or $_SESSION['logged'] != TRUE) {
		echo "<script>alert('Login to access!');</script>";
		session_destroy();
        header("Refresh:0; url=login.php");
        die();
	}

	if($_SESSION['browser'] != $_SERVER['HTTP_USER_AGENT']) {
		echo "<script>alert('Session hijacking detected');</script>";
		session_destroy();
        header("Refresh:0; url=login.php");
        die();
	}

?>
```
## /logout.php
```php
<?php
	session_start();
	// echo "<script>alert('You have logged out!');</script>";
	session_destroy();
    header("Location:Views/login.php");
    die();
?>
```
## /Post.php
```php
<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'authenticate_session.php');

	require($domain.'models/posts.php');

	if (isset($_POST) && !empty($_POST)) {
		switch ($_POST["form_type"]) {
			case 'Post':
				addPost($_POST);
				break;

			case 'Edit Post':
				editPost($_POST);
				break;

			case '':
				if (validateInput($_POST)) { 
					changePasswordHandler($_POST);
				}
				break;
			
			default:
				echo "<script>alert('Invalid!');</script>";
				break;
		}
	}
	if (isset($_GET) && !empty($_GET)) {
		// print_r($_GET);
		// die();
		$csrfToken = $_GET['csrfTokenPostDelete'];
		$postId = $_GET['post_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostDelete'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}
		if(validatePostId($postId)) {
		 	$response = deletePost($postId);
		 	if($response != false) {
				// echo "<script>alert('Post successfully Deleted!');</script>";
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		}
	}

	function validateInput($data) {
		if (empty($data['content'])) {
			error_log("miniFacebook: " . " post content is empty!");
			return FALSE;
		}
		return TRUE;
	}

	function validatePostId($postId) {

		if(empty($postId)) return FALSE;

		if(!is_numeric($postId)) return FALSE;

		if ($postId == 0) return FALSE;

		return TRUE;
	}

	function editPost($data) {
		$csrfToken = $data['csrfTokenPostEdit'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostEdit'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		 }
		if(validateInput($_POST) && !empty($_POST['postid'])) {
			$res = savePost($data); 
			if($res != false) {
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		} else{
			echo "<script>alert('Invalid post content');</script>";
			header("Location:Views/profile.php");
			die();
		}
		//echo "<script>alert('Post save failed!');</script>";
		header("Location:Views/prrofile.php");
	}

	function addPost($data) {
		$csrfToken = $data['csrfTokenPostCreate'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenPostCreate'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		 }
		if(validateInput($_POST)) {
			$res = insertPost($data); 
			if($res != false) {
				// echo "<script>alert('Post successfully saved!');</script>";
		        header("Refresh:0; url=Views/profile.php");
		        die();
			}
		} else{
			echo "<script>alert('Invalid post content');</script>";
			header("Location:Views/profile.php");
			die();
		}
		echo "<script>alert('Post save failed!');</script>";
		header("Location:Views/prrofile.php");
	}

	function getPostsData() {
		return getAllPosts();
	}

	function getStatistics() {
		return getStats();
	}

?>
```
## /Comment.php
```php
<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'authenticate_session.php');

	require('models/comments.php');

	// print_r($_POST);
	if (isset($_POST) && !empty($_POST)) {
		switch ($_POST["form_type"]) {
			case 'Comment':
				addPostComment($_POST);
				break;

			case 'Edit Comment':
				editPost($_POST);
				break;
			
			default:
				echo "<script>alert('Invalid!');</script>";
				break;
		}
	}

	if (isset($_GET) && !empty($_GET)) {
		// print_r($_GET);
		// die();
		$csrfToken = $_GET['csrfTokenCommentDelete'];
		$commentId = $_GET['comment_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenCommentDelete'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}
		if(validateCommentId($commentId)) {
			//check if user is post owneror comment owner
			$owner = getOwnerType($commentId);
			print_R($owner);
			if(!empty($owner)) {
			 	$response = deleteComment($commentId);
			 	if($response != false) {
					// echo "<script>alert('Post successfully Deleted!');</script>";
			        header("Refresh:0; url=Views/profile.php");
			        die();
				}
			}
		}
		echo "<script>alert('Comment delete failed! Try again');</script>";
        header("Refresh:0; url=Views/profile.php");
        die();
	}

	function validateCommentId($commentId) {

		if(empty($commentId)) return FALSE;

		if(!is_numeric($commentId)) return FALSE;

		if ($commentId == 0) return FALSE;

		return TRUE;
	}

	function addPostComment($data) {
		// print_r($data); die();
		$csrfToken = $data['csrfTokenCommentCreate'];
		$postId = $data['post_id'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenCommentCreate'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		}

		$res = addComment($data);
		if($res != false) {
			// echo "<script>alert('Post successfully saved!');</script>";
	        header("Refresh:0; url=Views/profile.php");
	        die();
		}
		echo "<script>alert('Failed to Comment!');</script>";
	        header("Refresh:0; url=Views/profile.php");
	        die();
	}

	function getComments($post_id){}
?>
```
## /index.php
```php
<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
	$LIFETIME = 15*60; // 15 minutes
	$PATH = "/team3"; 
	$DOMAIN = "192.168.56.104"; 
	$SECURE = TRUE;
	$HTTPONLY = TRUE;
	session_set_cookie_params($LIFETIME, $PATH, $DOMAIN, $SECURE, $HTTPONLY);

	session_start(); 

	$mysqli = new mysqli('localhost', 'minifb', 'team3', 'miniFacebook');
	if($mysqli->connect_errno) {
		printf("Database connection failed!");
		exit();
	}  

	if(isset($_POST["username"]) and isset($_POST["password"])) {
		if (secureChecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION['logged'] = TRUE;
			$_SESSION['username'] = $_POST['username'];
            $_SESSION['browser'] = $_SERVER['HTTP_USER_AGENT'];
		} else {
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	} else {
		if(!isset($_SESSION['logged']) or $_SESSION['logged'] != TRUE) {
			echo "<script>alert('Not logged in, login first');</script>";
			header("Refresh:0; url=form.php");
			die();
		}
		if($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
            echo "<script>alert('Session hijacking is detected');</script>";
            header("Refresh:0; url=form.php");
            die();
        }
	}
?>
<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
<a href="changepasswordform.php">Change Password</a>
<a href="logout.php">Logout</a>
<?php 
	function checklogin($username, $password) {
		$account = array("admin","1234");
		if (($username== $account[0]) and ($password == $account[1])) 
		  return TRUE;
		else return FALSE;
  	}
  	function checklogin_mysql($username, $password) {
		global $mysqli;
		$sql1 = "SELECT * from users where username = '$username' AND password= password('$password')";

		// echo "DEBUG> sql = ".$sql1;

		$result = $mysqli->query($sql1);
		if($result->num_rows == 1)
			return TRUE;
		return false;
  	}

  	function secureChecklogin($username, $password) {
		global $mysqli;
		$preparedSql = "SELECT * from users where email = ? AND password= md5(?);";

		if(!$stmt = $mysqli->prepare($preparedSql))
			echo "Prepared statement error";

		$stmt->bind_param("ss", $username, $password);
		// echo "DEBUG> sql = ".$sql1;

		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store result Error";

		$result = $stmt;

		if($result->num_rows == 1)
			return TRUE;
		return false;
  	}
?>

```
## /Auth.php
```php
<?php
	$domain = '/var/www/html/team3/src/';
	require($domain.'models/users.php');
  
	if (isset($_POST)) {

		if(isset($_POST["form_type"])) {
			switch ($_POST["form_type"]) {
				case 'Register':
					registrationHandler($_POST);
					echo '<script>alert("Registration unsuccessful. Try again!")</script>';
					header("Refresh:0; url=Views/register.php");
					die();
					break;

				case 'Login':
					if (validateInput($_POST)) { 
						loginHandler($_POST);
					}
					echo '<script>alert("Login unsuccessful. Try again!")</script>';
					header("Refresh:0; url=Views/login.php");
					die();
					break;

				case 'Change Password':
					if (validateInput($_POST)) { 
						changePasswordHandler($_POST);
					}
					echo '<script>alert("Cannot change password. Try again!")</script>';
					header("Refresh:0; url=Views/changePassword.php");
					die();
					break;
				
				default:
					echo "<script>alert('Invalid!');</script>";
					break;
			}
		} 
		die();
	}

	function registrationHandler($data) {
		$registerData = $data;
		$email 		  = $data["email"];
		$contact 	  = $data["contact"];
		$password 	  = $data["password"];
		$conpassword  = $data["repassword"];

		unset($data["nickname"], $data["contact"], $data["location"]);

		if(!validateAge($data['dob'])) {
			echo '<script>alert("User MUST be 13+ to access!")</script>';
			header("Refresh:0; url=Views/register.php");
			die();
		}
		if (validateInput($data) && 
			validatePassword($password, $conpassword) &&
			validateEmail($email)
		) {
			if (!empty($contact) && !validateContact($contact)) {
				error_log("miniFacebook: " . "Invalid Contact!");
				return;
			}
			error_log("miniFacebook: " . "Successfully Validated!" . $data['name']) ;
			$registerData['user_type'] = 'U';
			if(registerUser($registerData)) {
				header("Location:Views/login.php");
				die();
			}
		}
		echo '<script>alert("Registration unsuccessful. Try again!")</script>';
		header("Refresh:0; url=Views/register.php");
		die();
	}

	function loginHandler($data) {
		error_log(json_encode($data));
		if(validateInput($data) &&
			validateEmail($data['email'])) {
			$res = login($data); error_log(json_encode($res));
			if($res != false) {
				if($res['status'] != 'A') {
					echo "<script>alert('Account Disabled!');</script>";
					header("Refresh:0; url=Views/login.php");
					die();
				}
				manageSession($res);
				
				header("Refresh:0; url=Views/profile.php");
				die();
			}
		}
		echo "<script>alert('Log in failed!');</script>";
		header("Refresh:0; url=Views/login.php");
		die();

	}

	function changePasswordHandler($data) {
		// print_r($data); die();
		require('authenticate_session.php');
		$newpassword 	= $data['password'];
		$conNewpassword 	= $data['repassword'];
		$user_id 		= $_SESSION['user_id'];
		$csrfToken		= $data['csrfTokenChangePasswordForm'];

		if(!isset($csrfToken) or $_SESSION['csrfTokenChangePasswordForm'] != $csrfToken){
			echo "<script>alert('CSRF attack detected');</script>";
			die();
		 }

		if(validatePassword($newpassword, $conNewpassword) && changePasswordDB($user_id, $newpassword)) {
			echo '<script>alert("Password changed Successfully!")</script>';
			header("Refresh:0; url=Views/profile.php");
			die();
		} else {
			echo '<script>alert("Can not change password. Please Try again!")</script>';
			header("Refresh:0; url=Views/changePassword.php");
			die();
		}
	}

	function manageSession($res) {
		require('session_create.php');
		$_SESSION['logged']   	= TRUE;
		$_SESSION['username'] 	= $res['name'];
		$_SESSION['email'] 		= $res['email'];
		$_SESSION['user_id'] 	= $res['id'];
        $_SESSION['browser']  	= $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['role']		= $res["userType"];
        error_log(json_encode($_SESSION));
	}

	function validatePassword($password, $conpassword) {
		if ($password != $conpassword) {
			return FALSE;
		}
		if (strlen($password) < 8 || strlen($password) > 16) {
			return FALSE;
		}
		$pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$/";
		if(!preg_match($pattern, $password)) {
			return FALSE;
		}
		return TRUE;
	}

	function validateEmail($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  			$emailErr = "Invalid email format";
  			error_log("miniFacebook: " . $emailErr  ." : Email" . $email) ;
  			return FALSE;
		}
		return TRUE;
	}

	function validateInput($data) {
		foreach($data as $key => $input) {
			if (empty($input)) {
				error_log("miniFacebook: " . "$key field is empty!");
				return FALSE;
			}
			if ($key != 'gender' && $key != 'user_type' && strlen($input) < 5 && strlen($input) > 50) {
				error_log($key.' Invalid Input!');
				return FALSE;
			}
		} 
		return TRUE;
	}

	function validateContact($contact) {
		if (strlen($contact) != 10) {
			return FALSE;
		}
		if (!preg_match("/^(\.\d{2,4})?\s?(\d{10})$/", $contact)) {
			return FALSE;
		}
		return TRUE;
	}

	function validateAge($dob) {
		$birthdate = new DateTime($dob);
		$today   = new DateTime('today');
		$age = $birthdate->diff($today)->y;
		return $age < 13 ? FALSE : TRUE;
	}

?>
```
## /admin/authenticate_session.php
```php
<?php
	$domain = '/var/www/html/team3/src/';

	require($domain. 'session_create.php');

	if(!isset($_SESSION['logged']) or $_SESSION['logged'] != TRUE) {
		echo "<script>alert('Login to access!');</script>";
		session_destroy();
        header("Refresh:0; url=../login.php");
        die();
	}

	if($_SESSION['browser'] != $_SERVER['HTTP_USER_AGENT']) {
		echo "<script>alert('Session hijacking detected');</script>";
		session_destroy();
        header("Refresh:0; url=../Views/login.php");
        die();
	}
	// If not superuser redirect to the profile page
	if($_SESSION['role'] != 'S') { // s for superuser and u for user
		echo "<script>alert('Permission Denied! You do not have access to this page.');</script>";
        header("Refresh:0; url=../profile.php");
        die();
	}

?>
```
## /admin/User.php
```php
<?php
	$domain = '/var/www/html/team3/src/';

	require($domain.'admin/authenticate_session.php');

	require($domain.'models/users.php');

	require($domain.'models/posts.php');

	if (isset($_POST) && !empty($_POST)) {
		//print_r($_POST); die();
		addSuperuser($_POST);
	}

	if (isset($_GET) && !empty($_GET)) {
		$csrfToken = $_GET['csrfTokenSuperuser'];
		$user_id = $_GET['user_id'];
		$operation = $_GET['operation'];

		if($operation != 'D' && $operation != 'E') {
			echo "<script>alert('Invalid operation input!');</script>";
			die();
		}
		$operation = $operation == 'D'?'I':'A';
		if(!isset($csrfToken) or $_SESSION['csrfTokenSuperuser'] != $csrfToken){
			echo "<script>alert('CSRF attack detected!');</script>";
			die();
		}
		if(validateUserId($user_id)) {
		 	$response = operation($user_id, $operation);
		 	if($response != false) {
		        header("Refresh:0; url=../Views/admin/list.php");
		        die();
			}
		}
	}

	function validateUserId($postId) {

		if(empty($postId)) return FALSE;

		if(!is_numeric($postId)) return FALSE;

		if ($postId == 0) return FALSE;

		return TRUE;
	}

	function getUsersList() {
		return getUsers();
	}

	function getStatistics() {
		return getStats();
	}

	function addSuperuser($data) {
		// print_r($data); die();
		$registerData = $data;
		$email 		  = $data["email"];
		$contact 	  = $data["contact"];
		$password 	  = $data["password"];
		$conpassword  = $data["repassword"];

		unset($data["nickname"], $data["contact"], $data["location"], $data["gender"], $data["dob"]);

		if (validateInput($data) && 
			validatePassword($password, $conpassword) &&
			validateEmail($email) 
		) {
			if (!empty($contact) && !validateContact($contact)) {
				error_log("miniFacebook: " . "Invalid Contact!");
				return;
			}
			error_log("miniFacebook: " . "Successfully Validated!" . $data['name']) ;
			$registerData['user_type'] = 'S';
			if(registerUser($registerData)) {
				header("Location:../Views/admin/list.php");
				die();
			} else {
				echo '<script>alert("Registration unsuccessful. Try again!")</script>';
				header("Location:../Views/admin/list.php");
				die();
			}
		}
	}

	function validatePassword($password, $conpassword) {
		if ($password != $conpassword) {
			return FALSE;
		}
		if (strlen($password) < 8 || strlen($password) > 16) {
			return FALSE;
		}
		$pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$/";
		if(!preg_match($pattern, $password)) {
			return FALSE;
		}
		return TRUE;
	}

	function validateEmail($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  			$emailErr = "Invalid email format";
  			error_log("miniFacebook: " . $emailErr  ." : Email" . $email) ;
  			return FALSE;
		}
		return TRUE;
	}

	function validateInput($data) {
		foreach($data as $key => $input) {
			if (empty($input)) {
				error_log("miniFacebook: " . "$key field is empty!");
				return FALSE;
			}
			if ($key != 'gender' && $key != 'user_type' && strlen($input) < 5 && strlen($input) > 50) {
				error_log($key.' Invalid Input!');
				return FALSE;
			}
		} 
		return TRUE;
	}

	function validateContact($contact) {
		if (strlen($contact) != 10) {
			return FALSE;
		}
		if (!preg_match("/^(\.\d{2,4})?\s?(\d{10})$/", $contact)) {
			return FALSE;
		}
		return TRUE;
	}


?>
```
## /sessiontest.php
```php
<?php
session_start(); 
if(isset($_SESSION['views']))
    $_SESSION['views'] = $_SESSION['views']+ 1;
else{
    $_SESSION['views'] = 1;
}
echo "You have visited this page " . $_SESSION['views'] . " times"; 
?>

```
## /Views/register.php
```php
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <link href="icon.png" rel="icon">
      <title>Team 3 - miniFacebook</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
      <link rel = "stylesheet" href = "style.css">
   </head>

   <body class="container-auth">

      <div class="container">

           <div class="panel-activity_status" align="center">

               <div class="heading regForm">Registration</div><br>
			
               <form name="dform" method="POST" onsubmit="return validate()" action="../Auth.php">
                  <table class="regForm" style="font-size: 18px">
                     <tr>
                        <th>Name <span class="star">*</span><br><input type="text" name="name" class="input" maxlength="30" required pattern = "^[a-zA-Z ]*$" title="Name should only contain alphabets"></th>
                        <th>Nickname <br><input type="text" name="nickname" maxlength="15" class="input"></th>
                     </tr>
                     <tr>
                        <th>Email <span class="star">*</span><br><input type="email" name="email" id="myEmail"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="input" required></th>
                        <th>Contact <br><input type="text" name="contact" id="mycont" class="input" pattern="^(\+\d{2,4})?\s?(\d{10})$"></th>
                     </tr>
                     <tr>
                        <th rowspan="2">Location <br><textarea type="text" name="location" class="input" maxlength="50"></textarea></th>
                        <th>Date Of Birth <span class="star">*</span><br><input type="date" name="dob" class="input" id="dob" required></th>
                     </tr>   
                     <tr>
                        <th>
                           Gender <span class="star">*</span><br>
                           <select id="gn" name="gender" required>
                              <option value=""></option>
                              <option value="F">Female</option>
                              <option value="M">Male</option>
                              <option value="O">Other</option>
                        </th>
      					</tr>
                     <tr>
                        <th>Password <span class="star">*</span><br><input type="password" name="password" class="input" minlength="8" maxlength="16" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$" title="Password must have at least 8 and maximum 16 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE"  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
                        form.repassword.pattern = this.value;"></th>
                        <th>Confirm Password <span class="star">*</span><br><input type="password" name="repassword" class="input" minlength="8" maxlength="16" required  required title="Password does not match" onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"></th>
                     </tr>
                     <tr>
                        <th><br><span class="star">* Mandatory Fields</span><br></br>
                        <th><br>Already a User?  <a href="login.php">Sign In!</a><br></br>
                     </tr>
                     <tr class="subbtn">
                        <th  colspan="2"><input class="button" type="submit" name="form_type" value="Register"></th>
                     </tr>
                  </table>
               </form>
            </div>
			
			</div>
		
	</div>
      <script type="text/javascript">
         function getAge(dateString) {
             var today = new Date();
             var birthDate = new Date(dateString);
             var age = today.getFullYear() - birthDate.getFullYear();
             var m = today.getMonth() - birthDate.getMonth();
             if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                 age--;
             }
             return age;
         }
         $("#dob").on("blur", function() {
            console.log($(this).val(), "dobValue");
            console.log(getAge($(this).val()));
         });
      </script>
   </body>
</html>
```
## /Views/login.php
```php
<?php 
	$domain = '/var/www/html/team3/src/';

	require($domain. 'session_create.php');

	if(isset($_SESSION['logged']) && $_SESSION['logged'] == TRUE) {
		if($_SESSION['browser'] == $_SERVER['HTTP_USER_AGENT']) {
        header("Refresh:0; url=profile.php");
        die();
      }
	}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <link href="icon.png" rel="icon">
      <title>Team 3 - miniFacebook</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="style.css">
   </head>

   <body class="login_body">

   	<div class="content" align="center">
			<h4 class="title_heading">mini<span style="color:#F5A3F6">Facebook</span></h4>
			<h2 style="font-size:35px;color:white">CPS-575 Team Project</h2>
			<h2 style="font-size:20px;color:white">Instructor: Dr. Phu Phung</h2>
			<h2 style="font-size:25px;color:white">By Team 3: Nirmala Jadhav & Vrunda Patel</h2>
		</div>
      <div class="content" align="center">
			<form method="POST" action="../Auth.php">
			<table class="login" align="center" bgcolor="#70BBFF" cellpadding="10">
				<tr>
					<th colspan="2"><h2 id="login">Log In!</h2></th>
				</tr>
				<tr>
					<th>Email:</th>
					<td><input type="email" class="login_input" name="email" id="myEmail"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="input" required></td>
				</tr>
				<tr>
					<th>Password:</th>
					<td><input class="login_input" type="password" name="password" minlength="8" maxlength="16"></td>
				</tr>
				<tr>
					<th colspan="2"><input class="button" type="submit" value="Login" name="form_type">
				</tr>
				<tr>
					<td id="register">Don't have an account?</td>
					<td id="register"><a href="register.php">Sign Up!</a></td>
				</tr>
			</table>
		</form>
		</div>
      <script type="text/javascript"></script>
   </body>
</html>
```
## /Views/profile.php
```php
<?php
    require('../authenticate_session.php');

    require('../Post.php');

    /* Create Post CSRF token */
    $createRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostCreate"] = $createRand;

    /* Edit Post CSRF token */
    $editRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostEdit"] = $editRand;

    /* Delete Post CSRF token */
    $deleteRand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenPostDelete"] = $deleteRand;

    /* Create Comment CSRF token */
    $commRandCreate = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenCommentCreate"] = $commRandCreate;

    /* Delete Comment CSRF token */
    $commRandDelete = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenCommentDelete"] = $commRandDelete;

    $response  = getPostsData();

    $statistics = getStatistics(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='admin/list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Activity Feed</h3>
            </div>

            <div class="panel-content panel-activity">
                <!-- Add Post -->
                <form method="POST" action="../Post.php" class="panel-activity__status">
                    <input type="hidden" name="csrfTokenPostCreate" value="<?php echo $createRand; ?>">
                    <textarea name="content" placeholder="Share what you've been up to..." class="form-control" maxlength="200" required></textarea>
                    <input type="hidden" name="image_path">
                    <div class="actions">
                        <div class="btn-group">
                            
                        </div>
                        <button type="submit" class="btn btn-rounded btn-info" name="form_type" value="Post">
                            Post
                        </button>
                    </div>
                </form> <!-- End of Add Post -->

                <!-- Post List -->
                <ul class="panel-activity__list">
                <?php
                if(!empty($response)) {
                    
                    foreach ($response as $key => $value) {
                ?>
                <li>
                    <!-- Post Owner name and Action -->
                    <div class="activity__list__header">

                        <img src="postProfile.png" style="max-width: 40px;" alt="" />

                        <a href="#">
                            <?php 
                                echo "<span style=font-size:20px>".$value['name']."</span>"; 
                                if($value['user_type'] == 'S') {
                                    echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:15px' title='Superuser'></i>";
                                }
                            ?> 
                        </a>

                        <!-- Edit and Delete Options only to Owner -->
                        <?php
                            if($_SESSION['user_id'] == $value['user_id']) {
                        ?>
                            <!-- Edit Post Link--> 
                            <i class="fa fa-pencil editPostClass" id="postid_<?php echo $value['post_id']; ?>"></i>
                            <!-- Delete Post Link --> 
                            <a href="../Post.php?post_id=<?php echo $value['post_id']; ?>&csrfTokenPostDelete=<?php echo $deleteRand?> "><i class="fa fa-trash"></i></a>
                        <?php } ?>

                        <br>
                        <div style="color: black;" id="post_<?php echo $value['post_id']; ?>"><?php echo $value['content'];?>
                            <span style="float:right">
                                <?php echo date('d M Y', strtotime($value['created'])); ?>
                            </span>
                        </div>

                    </div> <!-- End of Post Owner and Action -->

                    <div class="activity__list__body entry-content">
                        <p>
                            <!-- Display the post content -->
                            <!-- <div id="post_<?php //echo $value['post_id']; ?>"><?php //echo $value['content'];?></div> -->

                            <!-- Edit post for owners only -->
                            <?php
                                if($_SESSION['user_id'] == $value['user_id']) {
                            ?>
                            <div id="edit_postid_<?php echo $value['post_id']; ?>" class="editPost">
                                <form method="POST" action="../Post.php" class="panel-activity__status">
                                    <input type="hidden" name="csrfTokenPostEdit" value="<?php echo $editRand; ?>">
                                    <input type="hidden" name="postid" value="<?php echo $value['post_id']; ?>">
                                    <input name="content" maxlength="200" required class="form-control" value="<?php echo $value['content']; ?>">
                                    <div class="actions">
                                        <button type="submit" class="btn btn-sm btn-rounded btn-info" name="form_type" value="Edit Post">Save</button>
                                    </div>
                                </form>
                                <br></br>
                            </div>
                            <?php
                                }
                            ?>
                            <!-- End of Edit post for owners only -->
                        </p>
                    </div>

                    <!-- List Comments -->
                    <div class="panel-activity__status">
                        <?php 
                            if(!empty($value['comments'])){
                        ?>
                                <ul class="actions panel-activity__list comment_list">
                        <?php
                                foreach ($value['comments'] as $comm) {
                        ?>
                                <li>
                                    <?php 
                                        echo "<b>".$comm['name'] ."</b>"; 
                                        if($comm['user_type'] == 'S') {
                                            echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:15px' title='Superuser'></i>";
                                        }

                                    ?>
                                    <?php
                                        if($_SESSION['user_id'] == $comm['user_id'] || $_SESSION['user_id'] == $value['user_id']) {
                                    ?> 
                                        <a style="color:black" href="../Comment.php?comment_id=<?php echo $comm['comment_id']; ?>&csrfTokenCommentDelete=<?php echo $commRandDelete?> "><i class="fa fa-trash"></i></a>
                                    <?php } ?>

                                    <?php 
                                    echo "<br><span style='padding-left:2%'>" . $comm['comment']."</span>"; ?>
                                    <span style="float:right">
                                    <?php echo date('d M Y', strtotime($comm['created'])); ?>
                                    </span>
                                </li>
                                <hr>
                                
                        <?php      }?>
                                </ul>
                        <?php  } ?>
                           
                    </div> <!-- End of List Comments -->

                    <!-- Add Comments -->
                    <form method="POST" action="../Comment.php" class="panel-activity__status">
                        <input type="hidden" name="csrfTokenCommentCreate" value="<?php echo $commRandCreate; ?>">
                        <input type="text" name="comment" placeholder="Share your comments..." class="form-control" required maxlength="200">
                        <input type="hidden" name="post_id" value="<?php echo $value['post_id']; ?>">
                        <div class="actions">
                
                            <button type="submit" class="btn btn-sm btn-rounded btn-info" name="form_type" value="Comment">
                                Comment
                            </button>
                        </div>
                    </form> <!-- End of Add Comments -->

                </li>
                <?php
                    }
                } else {
                    echo "No Posts";
                }
                ?>

                </ul> <!-- Post List -->

            </div>

        </div> <!-- End of Activity Feed -->
    </div>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.editPost').hide();

            $( ".editPostClass" ).click(function() {
                var postid = this.id.split("_")[1];
                // alert("show" + "#edit_postid_"+postid);
                $("#edit_postid_"+postid).show();
                $("#post_"+postid).hide();

            });

        });
    </script>
</body>
</html>
```
## /Views/admin/list.php
```php
<?php
    $domain = '/var/www/html/team3/src/';
    require($domain.'admin/authenticate_session.php');

    require($domain.'admin/User.php');

    /* Disable User CSRF token */
    $csrfTokenSuperuser = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenSuperuser"] = $csrfTokenSuperuser;


    $statistics = getStatistics(); 
    $response  = getUsersList();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "../style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        table {
            color: black;
            border: 1px solid black;
        }
        td, thead {
            text-align: center;
        }
        .bg {
            background-color: #d0a9ec80;
        }
        .rowbg {
            background-color: white;
        }
    </style>
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="../profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'] . " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../profile.php';"">
                    <i class="fa fa-user"></i>
                    <span>Profile</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Users List -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Users List</h3>
                <!-- <button class="btn btn-rounded btn-info" style="float:right;" onclick="document.location.href='superuser_register.php';">
                    <i class="fa fa-user-plus"></i>
                    <span>Add Superuser</span>
                </button> -->
            </div>

            <div class="panel-content panel-activity">
                <table class="table table-bordered table-hover">
                    <?php if(!empty($response)) {?>
                    <thead class="bg">
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        <?php foreach($response as $user) {
                            if($user['user_type'] == 'U') {?>
                        <tr class="rowbg">
                            <td><?php echo $user['name'];?> </td>
                            <td><?php echo $user['email'];?></td>
                            <td><?php echo date('d M Y, h:i A', strtotime($user['created']));?></td>
                            <td>
                                <!-- If enabled... add button disable -->
                                <?php if($user['status'] == 'A') {?>
                                    <a href="../../admin/User.php?user_id=<?php echo $user['user_id']; ?>&csrfTokenSuperuser=<?php echo $csrfTokenSuperuser?>&operation=D">
                                        <button class="btn btn-rounded btn-danger">
                                            Disable User
                                        </button>
                                    </a>
                                <?php 
                                    }
                                    else {
                                ?>
                                <!-- If disable... add button enable -->
                                    <a href="../../admin/User.php?user_id=<?php echo $user['user_id']; ?>&csrfTokenSuperuser=<?php echo $csrfTokenSuperuser?>&operation=E">
                                        <button class="btn btn-rounded btn-success">
                                            Enable User
                                        </button>
                                <?php } }?>

                            </td>
                        </tr>
                    <?php 
                        } // end of for loop to display users
                    ?>
                    </tbody>
                    <?php 
                        } // end of  empty response
                        else {
                            echo "No Users!";
                        } 
                    ?>
                </table>
            </div>

        </div> <!-- End of Users List -->

        <!-- Superusers List -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Superusers List</h3>
                <button class="btn btn-rounded btn-info" style="float:right;" onclick="document.location.href='superuser_register.php';">
                    <i class="fa fa-user-plus"></i>
                    <span>Add Superuser</span>
                </button>
            </div>

            <div class="panel-content panel-activity">
                <table class="table table-bordered table-hover">
                    <?php if(!empty($response)) {?>
                    <thead class="bg">
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Created</th>
                    </thead>
                    <tbody>
                        <?php foreach($response as $user) {
                            if($user['user_type'] == 'S') { ?>
                        <tr class="rowbg">
                            <td><?php echo $user['name'];?> </td>
                            <td><?php echo $user['email'];?></td>
                            <td><?php echo date('d M Y, h:i A', strtotime($user['created']));?></td>
                        </tr>
                    <?php 
                            } 
                        }// end of for loop to display superusers
                    ?>
                    </tbody>
                    <?php 
                        } // end of  empty response
                        else {
                            echo "No Users!";
                        } 
                    ?>
                </table>
            </div>

        </div> <!-- End of Superuser List -->
    </div>
    <script type="text/javascript">
    </script>
</body>
</html>
```
## /Views/admin/superuser_register.php
```php
<?php
   $domain = '/var/www/html/team3/src/';

   require($domain.'admin/authenticate_session.php');
   require('../../Post.php');
   $statistics = getStatistics();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "../style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="../profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            
            <div class="panel-content panel-activity">
                <div class="panel-activity_status" align="center">
                    <div class="heading regForm">Register Superuser</div><br>
         
                   <form name="dform" method="POST" onsubmit="return validate()" action="../../admin/User.php">
                          <table class="regForm" style="font-size: 18px">
                             <tr>
                                <th>Name <span class="star">*</span><br><input type="text" name="name" class="input" maxlength="30" required></th>
                                <th>Nickname <br><input type="text" name="nickname" maxlength="15" class="input"></th>
                             </tr>
                             <tr>
                                <th>Email <span class="star">*</span><br><input type="email" name="email" id="myEmail"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="input" required></th>
                                <th>Contact <br><input type="text" name="contact" id="mycont" class="input" pattern="^(\+\d{2,4})?\s?(\d{10})$"></th>
                             </tr>
                             <tr>
                                <th rowspan="2">Location <br><textarea type="text" name="location" class="input" maxlength="50"></textarea></th>
                                <th>Date Of Birth <span class="star">*</span><br><input type="date" name="dob" class="input" required></th>
                             </tr>   
                             <tr>
                                <th>
                                   Gender <span class="star">*</span><br>
                                   <select id="gn" name="gender" required>
                                      <option value=""></option>
                                      <option value="F">Female</option>
                                      <option value="M">Male</option>
                                      <option value="O">Other</option>
                                </th>
                             </tr>
                             <tr>
                                <th>Password <span class="star">*</span><br><input type="password" name="password" class="input" minlength="8" maxlength="16" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$" title="Password must have at least 8 and maximum 16 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE"  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
                                form.repassword.pattern = this.value;"></th>
                                <th>Confirm Password <span class="star">*</span><br><input type="password" name="repassword" class="input" minlength="8" maxlength="16" required  required title="Password does not match" onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"></th>
                             </tr>
                             <tr>
                                <th><br><span class="star">* Mandatory Fields</span><br></br>
                                <th><br>Already signed up?  <a href="login.php">Sign In!</a><br></br>
                             </tr>
                             <tr class="subbtn">
                                <th  colspan="2"><input class="button" type="submit" name="form_type" value="Register"></th>
                             </tr>
                          </table>
                   </form>
                </div>
            </div>

        </div> <!-- End of Activity Feed -->
    </div>
    
</body>
</html>
```
## /Views/changePassword.php
```php
<?php
    require('../authenticate_session.php');
    $rand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["csrfTokenChangePasswordForm"] = $rand;
    require('../Post.php');
    $statistics = getStatistics(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel = "stylesheet" href = "style.css"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <button class="btn btn-rounded btn-info" onclick="document.location.href='profile.php';"">
                    <i class="fa fa-user"></i>
                    <span>Profile</span>
                </button>
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='admin/list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../../chat.php';">
                    <i class="fa fa-comment"></i>
                    <span>Chat</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='../logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            
            <div class="panel-content panel-activity">
                <div class="panel-activity_status" align="center">
                    <div class="heading regForm">Change Password</div><br>
                    <form name="dform" method="POST" action="../Auth.php">
                      <input type="hidden" name="csrfTokenChangePasswordForm" value="<?php echo $rand; ?>">
                      <table class="regForm" style="font-size: 18px">
                         <tr>
                            <th>Password <span class="star">*</span><br><input type="password" name="password" class="input" minlength="8" maxlength="16" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,16}$" title="Password must have at least 8 and maximum 16 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE"  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
                            form.repassword.pattern = this.value;"></th>
                            <th>Confirm Password <span class="star">*</span><br><input type="password" name="repassword" class="input" minlength="8" maxlength="16" required  required title="Password does not match" onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"></th>
                         </tr>
                         <tr>
                            <th><br><span class="star">* Mandatory Fields</span><br></br>
                         </tr>
                         <tr class="subbtn">
                            <th  colspan="2"><input class="button" type="submit" name="form_type" value="Change Password"></th>
                         </tr>
                      </table>
                   </form>
                </div>
            </div>

        </div> <!-- End of Activity Feed -->
    </div>
    
</body>
</html>
```
## index.js ##
``` js
var https = require('https')
var fs = require('fs')
var xssfilter = require("xss")
var sslcertificate  = {
  key: fs.readFileSync('/etc/ssl/secad.key'), 
  cert: fs.readFileSync('/etc/ssl/secad.crt') //ensure you have these two files
};
var httpsServer = https.createServer(sslcertificate,httphandler);
var socketio = require('socket.io')(httpsServer);

httpsServer.listen(4430); //cannot use 443 as since it reserved for Apache HTTPS
console.log("HTTPS server is listenning on port 4430");

function httphandler (request, response) {
  response.writeHead(200); // 200 OK 
  var clientUI_stream = fs.createReadStream('./client.html');
  clientUI_stream.pipe(response);
}

socketio.on('connection', function (socketclient) {
  console.log("A new socket.IO client is connected: "+ socketclient.client.conn.remoteAddress+
               ":"+socketclient.id);

  socketclient.on("message", (data) => {
    console.log("Receved Data from client/browser: " + data);
    //if(socketclient.loggedin) {
    	var data = xssfilter(data);
    	console.log("Receved Data: " + data); 
    	socketio.emit("message", socketclient.username +" says: "+ data); 
    // }	
  });

  socketclient.on("typing",() => {
  	// console.log("Someone is typing...");
  	socketio.emit("typing", socketclient.username + " is typing...");
  });

  socketclient.on("online",(usernameChat) => {
    console.log("Someone is online...");
    socketclient.username = usernameChat;
    socketio.emit("online", usernameChat + " is online!");
  });

});
```
## chat.php ##
``` php
<?php
  require('authenticate_chat.php');
  require('./src/Post.php');
  $statistics = getStatistics();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link href="icon.png" rel="icon">
    <title>Team 3 - miniFacebook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="src/Views/style.css">
    <!-- <script src="https://secad-team3-jadhavn1.minifacebook.com:4430/socket.io/socket.io.js"></script> -->
    <script src="https://secad-team3-patelv27.minifacebook.com:4430/socket.io/socket.io.js"></script>
    <style>
      .chat {
        color: black;
        font-size: 20px;

      }
    </style>
  </head>

  <body onload="startTime()" class="profile_body">

    <div class="container">

        <!-- Profile Header -->
        <div class="panel profile-cover">
            <div class="profile-cover__img">
                <img src="./src/Views/profile.png" alt="" />
                <h3 class="h3"><b>
                <?php 
                    echo $_SESSION['username'];
                    if($_SESSION['role'] == 'S') {
                        echo " <i class='fa fa-star' style='color:#ff1fb2;font-size:20px' title='Superuser'></i>";
                    }
                ?></b></h3>
            </div>
            <div class="profile-cover__action bg--img" data-overlay="0.3">
                <?php 
                    if($_SESSION['role'] == 'S') {
                ?>
                        <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/admin/list.php';">
                            <i class="fa fa-book"></i>
                            <span>User List</span>
                        </button>

                <?php
                    }
                ?>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/profile.php';">
                    <i class="fa fa-comment"></i>
                    <span>Profile</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/Views/changePassword.php';">
                    <i class="fa fa-lock"></i>
                    <span>Change Password</span>
                </button>
                <button class="btn btn-rounded btn-info" onclick="document.location.href='src/logout.php';">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </button>
            </div>
            <div class="profile-cover__info">
            <?php
                if(!empty($statistics)) {
                    foreach ($statistics as $key => $value) {
            ?>
                <ul class="nav">
                    <li><strong><?php echo $value['count_user']; ?></strong>Users</li>
                    <li><strong><?php echo $value['count_post']; ?></strong>Posts</li>
                    <li><strong><?php echo $value['count_comment']; ?></strong>Comments</li>
                </ul>
             <?php
                    }
                }
            ?>
            </div>
        </div> <!-- End of Profile Header -->

        <!-- Activity Feed -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Public Chat</h3>
            </div>

            <div class="panel-content panel-activity chat">
              <input type="hidden" id="usernameChat" value="<?php echo $_SESSION['username'];?>">
              Current time: <div id="clock"></div>
              <br>
              Type message and press Enter to Send: <br><input type = "text" id="message" size = "30" onkeypress="entertoSend(event)" onkeyup="myWebSocket.emit('typing')"/>
              
              <br><br>
              <div id="typing"></div>
                  Messages:
              <hr>
              <div class="comment_list" id="receivedmessage"></div>
            </div>
        </div> <!-- End of Activity Feed -->
    </div>

    <script>
      var usernameChat = $("#usernameChat").val();
    
      function startTime() {
        document.getElementById('clock').innerHTML = new Date();
        // $('#clock').innerHTML = new Date();
        setTimeout(startTime, 500);
      }
      function doSend(msg){
          if (myWebSocket) {
            myWebSocket.send(msg);
            console.log('Sent to server: ' +msg);
          }
        }
        function entertoSend(e){
          //alert("keycode =" + e.keyCode);
          if(e.keyCode==13){//enter key
            doSend(document.getElementById("message").value);
            document.getElementById("message").value = "";
          }
        }
    // $(document).ready(function() {

      // var myWebSocket = io.connect('192.168.56.104:4430');
      var myWebSocket = io.connect('192.168.56.102:4430');

      myWebSocket.onopen = function() { 
        console.log('WebSocket opened'); 
      }

      myWebSocket.emit("online", usernameChat);

        var sanitizeHTML = function(str) {
          var temp = document.createElement('div');
          temp.textContent = str;
          return temp.innerHTML;
        } 

        myWebSocket.on("message", function(msg) {
          console.log('Received from server: '+ msg);
          chatMsg = sanitizeHTML(msg).split("says:");
          chatUser = "<b>"+ chatMsg[0] +"</b>" + " says : "  + chatMsg[1];
          document.getElementById("receivedmessage").innerHTML += chatUser + "<br>";
          // $('#receivedmessage').innerHTML += sanitizeHTML(msg) + "<br>";
        });

        myWebSocket.on("online", function(msg) {
          console.log('Received from server: '+ msg);
          document.getElementById("receivedmessage").innerHTML += sanitizeHTML(msg) + "<br>";

        });

        myWebSocket.on("typing", function(msg) {
          document.getElementById("typing").innerHTML = msg + "<br>";
          /*setsession_createTimeout(
            function(){
              document.getElementById("typing").innerHTML = '<br>';
            } , 500);*/

          setTimeout(function(){
            document.getElementById("typing").innerHTML= "<br"
          }, 1000); 


        });

        myWebSocket.onclose = function() { 
          console.log('WebSocket closed');
        }

    </script>

  </body>
</html>

```
## authenticate_chat.php ##
``` php
<?php
	$domain = '/var/www/html/team3/src/';

	require($domain. 'session_create.php');

	if(!isset($_SESSION['logged']) or $_SESSION['logged'] != TRUE) {
		echo "<script>alert('Login to access!');</script>";
		session_destroy();
        header("Refresh:0; url=src/Views/login.php");
        die();
	}

	if($_SESSION['browser'] != $_SERVER['HTTP_USER_AGENT']) {
		echo "<script>alert('Session hijacking detected');</script>";
		session_destroy();
        header("Refresh:0; url=src/Views/login.php");
        die();
	}

?>
```