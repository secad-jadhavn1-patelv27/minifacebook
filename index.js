var https = require('https')
var fs = require('fs')
var xssfilter = require("xss")
var sslcertificate  = {
  key: fs.readFileSync('/etc/ssl/secad.key'), 
  cert: fs.readFileSync('/etc/ssl/secad.crt') //ensure you have these two files
};
var httpsServer = https.createServer(sslcertificate,httphandler);
var socketio = require('socket.io')(httpsServer);

httpsServer.listen(4430); //cannot use 443 as since it reserved for Apache HTTPS
console.log("HTTPS server is listenning on port 4430");

function httphandler (request, response) {
  response.writeHead(200); // 200 OK 
  var clientUI_stream = fs.createReadStream('./client.html');
  clientUI_stream.pipe(response);
}

socketio.on('connection', function (socketclient) {
  console.log("A new socket.IO client is connected: "+ socketclient.client.conn.remoteAddress+
               ":"+socketclient.id);

  socketclient.on("message", (data) => {
    console.log("Receved Data from client/browser: " + data);
    //if(socketclient.loggedin) {
    	var data = xssfilter(data);
    	console.log("Receved Data: " + data); 
    	socketio.emit("message", socketclient.username +" says: "+ data); 
    // }	
  });

  socketclient.on("typing",() => {
  	// console.log("Someone is typing...");
  	socketio.emit("typing", socketclient.username + " is typing...");
  });

  socketclient.on("online",(usernameChat) => {
    console.log("Someone is online...");
    socketclient.username = usernameChat;
    socketio.emit("online", usernameChat + " is online!");
  });

});
